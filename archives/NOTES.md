# Notes

## 3 étapes

- location
- selection
- actions

## location

- déplacement au clavier en x,y

## selection

- unit où le curseur est placé
- 4 units autour de l'unit où le curseur est placé
- ...

## actions

### fill

- color
- pattern
- algorithm

### sort

- algorithm

### reverse

- algorithm
