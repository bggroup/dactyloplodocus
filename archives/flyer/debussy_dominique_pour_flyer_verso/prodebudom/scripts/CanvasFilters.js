class CanvasFilters{

    constructor(canvas){

        this.canvas = canvas;
        this.canvasContext = this.canvas.getContext("2d");
        this.pixels = this.getPixels();
    }

    getPixels(){
        return this.canvasContext.getImageData(0,0, this.canvas .width, this.canvas.height);
    }

    setPixels(pixels){
        this.canvasContext.putImageData(pixels, 0,0);
    }

    grayscale() {

        this.pixels = this.getPixels();
        var d = this.pixels.data;

        for (var i=0; i<d.length; i += 4){

            var r = d[i];
            var g = d[i+1];
            var b = d[i+2];
            // CIE luminance for the RGB
            // The human eye is bad at seeing red and blue, so we de-emphasize them.
            var v = 0.2126*r + 0.7152*g + 0.0722*b;
            d[i] = d[i+1] = d[i+2] = v;
        }
        this.setPixels(this.pixels);
    };

    brightness(adjustment) {

        this.pixels = this.getPixels();
        var d = this.pixels.data;

        for (var i=0; i<d.length; i+=4) {
            d[i] += adjustment;
            d[i+1] += adjustment;
            d[i+2] += adjustment;
        }
        this.setPixels(this.pixels);
    };

    threshold(threshold) {

        this.pixels = this.getPixels();
        var d = this.pixels.data;

        for (var i=0; i<d.length; i+=4) {
            var r = d[i];
            var g = d[i+1];
            var b = d[i+2];
            var v = (0.2126*r + 0.7152*g + 0.0722*b >= threshold) ? 255 : 0;
            d[i] = d[i+1] = d[i+2] = v;
        }
        this.setPixels(this.pixels);
    };

    blur(distance){

        let dist = distance || 3;
        let matrix = [];

        for(let i=0; i<dist*dist; i++){
            matrix[i] = 1/(dist*dist);
        }

        this.convolute(matrix);
    }

    gaussianBlur(length, weight){

        if(!length){
            length = 3;
        }
        if(!weight){
            weight = 4;
        }

        let kernel = []; 
        let sumTotal = 0;
        let matrix = [];

        let kernelRadius = Math.floor(length / 2); 
        let distance = 0; 

        let calculatedEuler = 1.0 / (2.0 * Math.PI * Math.pow(weight, 2)); 

        for (let filterY = Math.floor(-kernelRadius); filterY <= kernelRadius; filterY++) 
        {
            kernel[filterY + kernelRadius] = [];

            for (let filterX = Math.floor(-kernelRadius); filterX <= kernelRadius; filterX++) 
            {
                distance = ((filterX * filterX) + (filterY * filterY)) / (2 *(weight * weight)); 

                kernel[filterY + kernelRadius][filterX + kernelRadius] =  
                    calculatedEuler * Math.exp(-distance); 

                sumTotal += kernel[filterY + kernelRadius][  
                    filterX + kernelRadius]; 
            } 
        } 

        for (let y = 0; y < length; y++){

            for (let x = 0; x < length; x++){

                kernel[y][x] = kernel[y][x] * (1.0 / sumTotal); 
                //flatten the kernel to single dimension array
                matrix.push(kernel[y][x]);
            } 
        }

        console.log(kernel);
        console.log(matrix);

        this.convolute(matrix);
    }

    sharpen(val){

        let sideVal = -((val-1)/4);
        console.log(sideVal);

        this.convolute([0, sideVal,  0,
                        sideVal, val, sideVal,
                        0, sideVal,  0]);

    }

    slide(val){

        this.convolute([0, val,  0,
                        -val, 1, -val,
                        0, val,  0]);

    }

    convolute(weights, opaque) {

        this.pixels = this.getPixels();

        var side = Math.round(Math.sqrt(weights.length));
        var halfSide = Math.floor(side/2);

        var src = this.pixels.data;
        var sw = this.pixels.width;
        var sh = this.pixels.height;

        // pad output by the convolution matrix
        var w = sw;
        var h = sh;

        var tmpCanvas = document.createElement('canvas');
        var output = tmpCanvas.getContext('2d').createImageData(this.canvas.width, this.canvas.height);
        var dst = output.data;

        // go through the destination image pixels
        var alphaFac = opaque ? 1 : 0;

        for (var y=0; y<h; y++) {

            for (var x=0; x<w; x++) {

                var sy = y;
                var sx = x;
                var dstOff = (y*w+x)*4;

                // calculate the weighed sum of the source image pixels that
                // fall under the convolution matrix
                var r=0, g=0, b=0, a=0;

                for (var cy=0; cy<side; cy++) {

                    for (var cx=0; cx<side; cx++) {

                        var scy = sy + cy - halfSide;
                        var scx = sx + cx - halfSide;

                        if (scy >= 0 && scy < sh && scx >= 0 && scx < sw) {

                            var srcOff = (scy*sw+scx)*4;
                            var wt = weights[cy*side+cx];
                            r += src[srcOff] * wt;
                            g += src[srcOff+1] * wt;
                            b += src[srcOff+2] * wt;
                            a += src[srcOff+3] * wt;
                        }
                    }
                }

                dst[dstOff] = r;
                dst[dstOff+1] = g;
                dst[dstOff+2] = b;
                dst[dstOff+3] = a + alphaFac*(255-a);
            }
        }
        this.setPixels(output);
    };

}