class CanvasPrinter{

    constructor(params){

        this.textArea = params.textArea;
        this.computedStyle = getComputedStyle(this.textArea);
        //console.log(this.computedStyle);

        //make the canvas
        this.canvas = document.createElement("canvas");
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;

        this.canvasContext = this.canvas.getContext("2d");

        this.canvas.style.zIndex = -1;
        this.canvas.style.position = "absolute";
        this.canvas.style.left = "0px";
        this.canvas.style.top = "0px";
        this.canvas.style.pointerEvents = "none";
        document.body.insertBefore(this.canvas, this.textArea);

        //we use it to reproduce the inner margin that html element automatically adds
        this.xOffsetBase = 2;

        this.color = "black";
        
        this.canvasContext.fillStyle = "white";
        this.canvasContext.fillRect(0,0, this.canvas.width, this.canvas.height);
        
    }

    //this.drawTextFromTextArea(textArea);

    setTextArea(val){
        this.textArea = val;
    }

    setLiveCodingRef(val){
        this.liveCodingTextArea = val;
    }

    print(){
        this.drawTextFromTextArea();
    }

    erase(){

        this.canvasContext.clearRect(0,0, this.canvasContext.canvas.width, this.canvasContext.canvas.height);

    }

    drawTextFromTextArea(){

        var baseString = this.textArea.value;

        //some font & line props computation

        var fontSizeString = this.computedStyle["font-size"];
        var fontSize = Number( fontSizeString.substring(0, fontSizeString.length-2) );

        //update the glyph size from live coding input
        this.glyphSize = this.liveCodingTextArea.computeGlyphSize(fontSize);

        var lineHeight = this.glyphSize.height;

        var lineHeightMargin = lineHeight - fontSize;

        this.lineWidth = this.liveCodingTextArea.lineWidth;

        this.canvasContext.font = this.computedStyle["font-size"] + " " + this.computedStyle["font-family"];

        //position x y as canvas need to construct text from scratch
        var yOffset = lineHeight-lineHeightMargin;
        var xOffset = this.xOffsetBase;

        this.canvasContext.fillStyle = this.color;

        if(baseString.length > 0){

            let charBuffer = [];

            //loop through every chars in string
            for(var i=0; i<baseString.length; i++){

                //get one char !won't work with non alphabetic languages!
                var char = baseString[i];

                //manage line feed
                if(char === "\n"){

                    charBuffer.push({char: char, xOffset: xOffset, yOffset: yOffset});
                    yOffset += lineHeight;
                    xOffset += this.glyphSize.width;

                    for(let i in charBuffer){
                        this.canvasContext.fillText(charBuffer[i].char, charBuffer[i].xOffset, charBuffer[i].yOffset);
                    }
                    charBuffer = [];
                    //reset offset for new line
                    xOffset = this.xOffsetBase;

                }else if(char === " " || i === baseString.length-1){

                    charBuffer.push({char: char, xOffset: xOffset, yOffset: yOffset});
                    xOffset += this.glyphSize.width;

                    for(let i in charBuffer){
                        this.canvasContext.fillText(charBuffer[i].char, charBuffer[i].xOffset, charBuffer[i].yOffset);
                    }
                    charBuffer = [];
                }else{

                    //this.canvasContext.fillText(char, xOffset, yOffset);
                    charBuffer.push({char: char, xOffset: xOffset, yOffset: yOffset});

                    //compute next offsets
                    xOffset += this.glyphSize.width;

                    //manage line break
                    if(xOffset > this.lineWidth - this.glyphSize.width){
                        
                        yOffset += lineHeight;
                        //reset xOffset for the word in buffer
                        xOffset = this.xOffsetBase;

                        for(let i in charBuffer){
                            charBuffer[i].xOffset = xOffset;
                            charBuffer[i].yOffset = yOffset;
                            xOffset += this.glyphSize.width;
                        }
                    }
                }
            }
        }
    }
}