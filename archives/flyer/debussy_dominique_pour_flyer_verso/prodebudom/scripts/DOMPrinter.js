class DOMPrinter{

    constructor(params){

        this.textArea = params.textArea;
        this.computedStyle = getComputedStyle(this.textArea);
        //console.log(this.computedStyle);

        //make the container
        this.htmlContainer = document.createElement("div");

        this.htmlContainer.style.position = "absolute";
        this.htmlContainer.style.left = "0px";
        this.htmlContainer.style.top = "0px";
        this.htmlContainer.style.pointerEvents = "none";
        this.htmlContainer.style.width = "100%";
        this.htmlContainer.style.height = "100%";

        setElementPerspective(this.htmlContainer);

        document.body.insertBefore(this.htmlContainer, this.textArea);

        this.htmlElementType = "pre";
        this.htmlElements = [];

        //we use it to reproduce the inner margin that html element automatically adds
        this.xOffsetBase = 2;
        this.yOffsetBase = 2;

        this.color = "black";
    }

    setTextArea(val){
        this.textArea = val;
    }

    setLiveCodingRef(val){
        this.liveCodingTextArea = val;
    }

    print(){
        this.createFromTextArea();
    }

    erase(){

        for(let i=0; i<this.htmlElements.length; i++){
            this.htmlElements[i].remove();
        }

    }

    createFromTextArea(){

        var baseString = this.textArea.value;

        //some font & line props computation

        var fontSizeString = this.computedStyle["font-size"];
        var fontSize = Number( fontSizeString.substring(0, fontSizeString.length-2) );

        //update the glyph size from live coding input
        this.glyphSize = this.liveCodingTextArea.computeGlyphSize(fontSize);

        var lineHeight = this.glyphSize.height;

        var lineHeightMargin = lineHeight - fontSize;

        this.lineWidth = this.liveCodingTextArea.lineWidth;

        //position x y as canvas need to construct text from scratch
        var yOffset = this.yOffsetBase;//lineHeight-lineHeightMargin;
        var xOffset = this.xOffsetBase;

        var el = document.createElement(this.htmlElementType);
        el.classList.add("liveCodingFont");
        el.style.fontSize = fontSizeString;
        el.style.position = "absolute";
        el.style.whiteSpace = "pre";
        el.style.color = this.color;

        /*el.style.left = xOffset + "px";
        el.style.top = yOffset + "px";*/

        el.transform = new Transform(el);
        el.transform.position.x = xOffset;
        el.transform.position.y = yOffset;
        el.transform.apply();

        this.htmlElements.push(el);
        this.htmlContainer.appendChild(el);

        var afterLineFeed = false;

        if(baseString.length > 0){

            //loop through every chars in string
            for(var i=0; i<baseString.length; i++){

                //get one char !won't work with non alphabetic languages!
                var char = baseString[i];

                //manage line feed
                if(char === "\n"){

                    xOffset = this.xOffsetBase;
                    yOffset += lineHeight;

                    afterLineFeed = true;

                }else if(char === " "){

                    //compute next offsets
                    xOffset += this.glyphSize.width;
                    afterLineFeed = true;//we need to create a new html element!

                    //manage line break
                    if(xOffset > this.lineWidth /*- this.glyphSize.width*/){

                        xOffset = this.xOffsetBase;
                        yOffset += lineHeight;

                        /*el.style.left = xOffset + "px";
                        el.style.top = yOffset + "px";*/

                        el.transform.position.x = xOffset;
                        el.transform.position.y = yOffset;
                        el.transform.apply();
                        
                        //update xOffset with new element's width
                        xOffset += el.offsetWidth;
                        console.log("xOffset", xOffset, el);
                    }

                    el.innerHTML += char;

                }else{
                    //we detected a linefeed sign or a space so create a new element
                    if(afterLineFeed){

                        el = document.createElement(this.htmlElementType);
                        el.style.position = "absolute";
                        el.classList.add("liveCodingFont");
                        el.style.fontSize = fontSizeString;
                        /*el.style.top = yOffset + "px";
                        el.style.left = xOffset + "px";*/
                        el.style.whiteSpace = "pre";
                        el.style.color = this.color;

                        el.transform = new Transform(el);
                        el.transform.position.x = xOffset;
                        el.transform.position.y = yOffset;
                        el.transform.apply();

                        this.htmlElements.push(el);
                        this.htmlContainer.appendChild(el);
                    }

                    //console.log("el.style.top",yOffset + "px", el.style.top);
                    afterLineFeed = false;

                    el.innerHTML += char;

                    //compute next offsets
                    xOffset += this.glyphSize.width;

                    //manage line break
                    if(xOffset > this.lineWidth /*- this.glyphSize.width*/){
                        yOffset += lineHeight;
                        xOffset = this.xOffsetBase;
                        /*el.style.top = yOffset + "px";
                        el.style.left = xOffset + "px";*/
                        el.transform.position.x = xOffset;
                        el.transform.position.y = yOffset;
                        el.transform.apply();

                        //update xOffset with new element's width
                        xOffset += el.offsetWidth;
                    }
                }
            }
        }
    }
};