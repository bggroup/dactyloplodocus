class AudioRenderer{

    constructor(params){

        if(!params){
            params = {};
        }

        if (window.AudioContext !== undefined)
        {
            this.audioContext = new window.AudioContext();
        }
        else if (window.webkitAudioContext !== undefined)
        {
            this.audioContext = new window.webkitAudioContext();
        }

        //hack to reset the SR
        if (this.audioContext)
        {
            if (this.audioContext.sampleRate !== 44100)
            {
                var buffer = this.audioContext.createBuffer(1, 1, 44100);
                var dummy = this.audioContext.createBufferSource();
                dummy.buffer = buffer;
                dummy.connect(this.audioContext.destination);
                dummy.start(0);
                dummy.disconnect();
                this.audioContext.close();

                if (window.AudioContext !== undefined)
                {
                    this.audioContext = new window.AudioContext();
                }
                else if (window.webkitAudioContext !== undefined)
                {
                    this.audioContext = new window.webkitAudioContext();
                }
            }

            this.analyser = this.audioContext.createAnalyser();
            this.fftSize = params.fftSize || 256;
            this.analyser.fftSize = this.fftSize;
            var bufferSize = this.analyser.frequencyBinCount;
            this.analyserArray = new Uint8Array(bufferSize);

            this.audioOutputNode = this.audioContext.createGain();
            this.audioOutputNode.connect(this.audioContext.destination);
        }

        this.listener = this.audioContext.listener;

    }

    setListenerTransform(transform)
    {
        this.listener.transform = transform;
    }
}

class AudioInput{

    constructor(params){

        this.renderer = params.renderer || undefined;
        this.audioContext = this.renderer.audioContext;
        this.openedCallback = params.openedCallback || undefined;
        this.inputLabel = params.inputLabel || "Micro intégré"; //Entrée intégrée || Micro intégré

        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia;
        navigator.getUserMedia( {audio: true}, () => {
            navigator.mediaDevices.enumerateDevices().then( this.devicesFound.bind(this) );
        }, this.streamError );

    }

    devicesFound(devices){

        let micId;

        for(let i in devices){

            let device = devices[i];

            if(device.kind === "audioinput"){

                console.log(device);

                if(device.label === this.inputLabel){

                    //we have a label comming from the OS, use it
                    micId = device.deviceId

                }else if(device.label === ""){

                    //no label... try to guess
                    if(this.inputLabel === "Micro intégré"){

                        micId = device.deviceId;
                        break;
                    }else{

                        micId = device.deviceId;//no break = will take the last input
                    }
                }
            }
        }

        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia;
        navigator.getUserMedia( {audio: {deviceId: micId} }, this.streamOpened.bind(this), this.streamError );

    }

    // success callback when requesting audio input stream
    streamOpened(stream) {

        console.log(this.renderer);

        // Create an AudioNode from the stream.
        let mediaStreamSource = this.audioContext.createMediaStreamSource( stream );

        // Connect it to the destination to hear yourself (or any other node for processing!)
        //mediaStreamSource.connect( this.audioContext.destination );
        mediaStreamSource.connect(this.renderer.analyser);

        if(this.openedCallback){
            this.openedCallback(this);
        }
    }

    streamError(err){
        console.error(err);
    }
}

/**
* AudioBuffer Class. Encapsulates the concept of buffer of Audio data. You can load, depending on the platform, various audio file formats, like WAV, MP3 or OGG. See the Web Audio API documentation. requires the Web Audio API.

* @class AudioBuffer
* @constructor
* @param {Object} params the parameters object
* @param {RendererAudio} params.renderer The audio renderer
* @param {ArrayBuffer} params.arrayBuffer the raw arraybuffer, already loaded, to give to the audio context
* @param {Function} params.decodedCallback the callback function to call when Audio Context has decoded the sound file's raw  arrayBuffer
*/
class AudioBuffer{

    constructor(params){

        this.renderer = params.renderer || undefined;
        this.arrayBuffer = params.arrayBuffer || undefined;
        this.decodedCallback = params.decodedCallback || undefined;

        if(this.arrayBuffer)
        {
            var context = this.renderer.audioContext;

            context.decodeAudioData(this.arrayBuffer,function(buffer){
                this.buffer = buffer;
                this.decodedCallback();

            }.bind(this));
        }
    }
    /**
    * Return the underlying WebAudio audio buffer
    * @method getInternalAudioBuffer
    * @return {Buffer} webAudio buffer
    */
    getInternalAudioBuffer()
    {
        return this.buffer;
    }
}


class AudioSource{
    /**
     * AudioSource Class. Encapsulates the concept of Audio source, a sound-emitting entity with a given position in space. requires the Web Audio API.
     *  
     * @class AudioSource
     * @constructor
     * @extends Component
     * @param {Object} params the parameters object
     * @param {RendererAudio} params.renderer The AudioRenderer 
    */

    constructor(params){

        this.renderer = params.renderer || undefined;
        this.panner;

        if (this.renderer.audioContext !== undefined)
        {
            this.panner = this.renderer.audioContext.createPanner();
            this.panner.panningModel = 'HRTF';
            this.panner.distanceModel = 'exponential';
            this.panner.refDistance = 10;
            this.panner.maxDistance = 10000;
            this.panner.rolloffFactor = 1;
            this.panner.coneInnerAngle = 360;
            this.panner.coneOuterAngle = 0;
            this.panner.coneOuterGain = 0;
        }
        this.is3D = false;
        this.loop = false;
    };

    /**
    * Sets the reference distance for sound attenuation through space
    * @method setRefDistance
    * @param {Number} val
    */
    setRefDistance(val)
    {
        this.panner.refDistance = val;
    }

    /**
    * Sets the maximum distance for sound propagation in space
    * @method setMaxDistance
    * @param {Number} val
    */
    setMaxDistance(val)
    {
        this.panner.maxDistance = val;
    }

    /**
     set the current AudioSource buffer
     @method setBuffer
     @param {Object} buffer the AudioBuffer we want the source to play.
     */
    setBuffer(buffer)
    {
        this.buffer = buffer;
    }

    /**
    * set 3D propagation
    * @method set3D
    * @param {Boolean} enabled
    */
    set3D(enabled)
    {
        this.is3D = enabled;
    }

    /**
     set the current Transform that represents the AudioSource position in space.
     @method setTransform
     @param {Object} transform the Transform.
     */
    setTransform(transform)
    {
        this.transform = transform;
    }

    /**
    * set this source loop
    * @method setLoop
    * @param {Boolean} enabled
    */
    setLoop(val)
    {
        this.loop = val;
    }

    /**
    * Updates the source's position in space if given from a transform
    * @method update
    */
    update()
    {
        if (this.transform !== undefined)
        {
            var pos = this.transform.getLocalPosition();
            this.panner.setPosition(pos.x, pos.y, pos.z);
        }
    }

    /**
     plays the AudioSource
     @method play
     */
    play()
    {
        if (this.renderer.audioContext !== undefined)
        {
            if (this.source !== undefined)
            {
                this.stop();
            }
            this.source = this.renderer.audioContext.createBufferSource();

            if (this.is3D)
            {
                this.source.connect(this.panner);
                this.panner.connect(this.renderer.audioContext.destination);
            }
            else
            {
                this.source.connect(this.renderer.audioContext.destination);
            }
            this.source.buffer = this.buffer.buffer;

            this.source.connect(this.renderer.analyser);
        }
        if (this.source !== undefined)
        {
            this.playing = true;

            if(this.loop){
                this.source.loop = true;
            }

            this.source.start(0);
        }
    }

    /**
     pauses the AudioSource.
     @method pause
     */
    pause()
    {
        if (this.source !== undefined)
        {
            this.playing = false;
        }
    }

    /**
     stops the AudioSource.
     @method stop
     */
    stop()
    {
        if (this.source !== undefined)
        {
            this.source.stop(0);
            this.source = undefined;
            this.playing = false;
        }
    }
}