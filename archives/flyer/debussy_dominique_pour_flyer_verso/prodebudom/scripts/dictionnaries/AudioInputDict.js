class AudioInputDict extends BaseDict{

    constructor(canvasPrinter, DOMPrinter){

        super(canvasPrinter, DOMPrinter);

        let listenWords = ["écoute", "écouter", "ecoute", "ecouter", "entend", "entendre"];
        let showFreqWords = ["visualisation"];
        let activeWords = ["actif", "active", "marche", "visible"];
        let deactiveWords = ["inactif", "inactive", "invisible"];

        this.keywords = {
            visualize: showFreqWords,
            listen: listenWords
        };

        this.modifiers = {
            visuActive: {keyword: "visualize", operation: "visualizeOn", words: activeWords},
            visuDeactive: {keyword: "visualize", operation: "visualizeOff",  words: deactiveWords},
            listenActive: {keyword: "listen", operation: "listenOn", words: activeWords},
            listenDeactive: {keyword: "listen", operation: "listenOff",  words: deactiveWords},
        };

        this.operations = {
            listenOn: this.listen.bind(this, true),
            listenOff: this.listen.bind(this, false),
            visualizeOn: this.visualize.bind(this, true),
            visualizeOff: this.visualize.bind(this, false)
        };

        super.processWordsAndModifiers();

        this.active = false;
        this.peakCallbacks = [];

        //AUDIO
        this.audioRenderer = new AudioRenderer();
        //these values should be overriden by config loading just after instanciation!
        this.peakLimit = 30;
        this.peakTemporizer = 1000/50;

        //special canvas for debuging
        this.visualizerCanvas = document.createElement("canvas");
        this.visualizerCanvas.width = canvasPrinter.canvas.width;
        this.visualizerCanvas.height = canvasPrinter.canvas.height;
        this.visualizerCanvas.style.position = "absolute";
        this.visualizerCanvas.style.left = "0px";
        this.visualizerCanvas.style.pointerEvents = "none";
        document.body.insertBefore(this.visualizerCanvas, DOMPrinter.htmlContainer);
    }

    /**
     * "live" || url
     */
    setAudioInput(val){

        if(val === "live"){

            //live stream
            this.audioInput = new AudioInput({renderer: this.audioRenderer,
                                              openedCallback: this.onAudioStreamOpened.bind(this),
                                              fftSize: 32,
                                              inputLabel: "Entrée intégrée"});

        }else{

            let url = val;
            this.audioBuffer = undefined;
            this.audioSource = new AudioSource({renderer: this.audioRenderer});

            var request = new XMLHttpRequest();
            request.open("GET", url, true);
            request.responseType = "arraybuffer";

            request.onload = function(e){

                var arrayBuffer = request.response; // Note: not request.responseText!

                this.audioBuffer = new AudioBuffer({renderer: this.audioRenderer,
                                                    arrayBuffer : arrayBuffer,
                                                    decodedCallback: this.onLoadAudioBuffer.bind(this)});

            }.bind(this);

            request.send(null);

        }

    }

    onLoadAudioBuffer(){

        this.audioSource.setBuffer(this.audioBuffer);
        this.audioSource.play();

        this.update();
    }

    onAudioStreamOpened(audioInput){

        this.update();
    }

    listen(val){

        if(val){
            this.timer = setInterval(this.update.bind(this), this.fps);
            this.active = true;
            
        }else{

            clearInterval(this.timer);
            this.active = false;
        }
    }

    visualize(val){

        this.visualizeOn = val;
        console.log("this.visualizeOn",this.visualizeOn);

        if(this.visualizeOn){
            this.listen(true);
            this.visualizerCanvas.style.visibility = "visible";
        }else{

            this.visualizerCanvas.style.visibility = "hidden";
        }
    }

    addPeakCallback(callback){

        this.peakCallbacks.push(callback);
    }

    releasePeak(){

        this.peakReleased = true;
    }

    /**
    * General update for all processes that needs it
    * @method update
    */
    update(){

        let analyser = this.audioRenderer.analyser;
        let analyserArray = this.audioRenderer.analyserArray;

        if(analyser && analyserArray){

            analyser.getByteFrequencyData(analyserArray);

            let sum = 0;

            if(this.visualizeOn){
                var ctx = this.visualizerCanvas.getContext("2d");

                ctx.fillStyle = "black";
                ctx.clearRect(0,0, ctx.canvas.width, ctx.canvas.height);

                var xStep = ctx.canvas.height / analyserArray.length;
            }

            for(var i=0; i<analyserArray.length; i++){

                if(this.visualizeOn){
                    
                    let x = i*xStep;
                    let y = map(analyserArray[i], 0, 300, 0, ctx.canvas.width);

                    ctx.beginPath();
                    ctx.moveTo(0, x);
                    ctx.lineTo(y, x);
                    ctx.stroke();
                }

                sum += analyserArray[i];
            }

            sum /= analyser.frequencyBinCount;

            if(sum > this.peakLimit && this.peakReleased){
                
                console.log("peaked with "+sum);

                this.peakReleased = false;
                setTimeout(this.releasePeak.bind(this), this.peakTemporizer);

                //trigger the callback added by other dict, if any
                for(let i in this.peakCallbacks){

                    let callback = this.peakCallbacks[i];

                    if(typeof callback === "function"){
                        callback();
                    }
                }
            }

            if(this.visualizeOn){

                ctx.fillText(sum, ctx.canvas.width/2, ctx.canvas.height/2);
            }
        }
    }
};