class BaseDict{

    constructor(canvasPrinter, DOMPrinter){

        this.repeatable = false;

        this.canvasPrinter = canvasPrinter;
        this.DOMPrinter = DOMPrinter;

        this.keywords;//object, filled by subclass
        this.words;//flat array of words

        this.fps = 1000/30;

        //this.regExp = "";
        this.matches = [];

        //blanc, bleu cobalt clair, vert émeraude, outremer, vermillon (rarement), jaune cadmium clair, et jaune citron.
        this.colorDictionnary = {
            rouge: "OrangeRed",
            rouges: "OrangeRed",
            blanc: "white",
            blancs: "white",
            blanche: "white",
            blanches: "white",
            bleu: "CornflowerBlue",
            bleue: "CornflowerBlue",
            bleus: "CornflowerBlue",
            bleues: "CornflowerBlue",
            outremer: "darkblue",
            gris: "grey",
            grise: "grey",
            grises: "grey",
            vert: "SpringGreen",
            verts: "SpringGreen",
            verte: "SpringGreen",
            vertes: "SpringGreen",
            jaune: "yellow",
            jaunes: "yellow",
            noir: "black",
            noirs: "black",
            noire: "black",
            noires: "black",
        };

        this.colorsWords = Object.keys(this.colorDictionnary);

    }

    /**
    * process effects keywords and modifiers words to use them in strings comparing
    * @method processWordsAndModifiers
    */
    processWordsAndModifiers(){

        let words = Object.values(this.keywords);
        //get a regExp
        this.words = this.generateFlatArray(words);

        let temp = Object.values(this.modifiers);
        let modifiersWords = [];
        temp.forEach( (el) => {
            modifiersWords.push(el.words);
        });
        //console.log("modifiersWords", modifiersWords);
        this.modifiersWords = this.generateFlatArray(modifiersWords);
    }

    /**
    * Check for words in the dictionnary in the given sentence
    * @method checkKeywords
    * @param {Object} sentence
    */
    checkKeywords(sentence){

        let matches = [];

        for(let t in sentence){

            let token = sentence[t];

            for(let i in this.words){

                let keyword = this.words[i];

                if(token.string.toLowerCase() === keyword){

                    matches.push(keyword);

                    let func = this.findOperationFromWord(keyword);

                    if(func){

                        //found a keyword defining an effect, search for its modifier
                        //modifiers are called internally to this function
                        this.checkModifiers(func.key, sentence);

                        console.log("calling effect", func.operation);
                        if(func.operation){ //some keyword can lead to no functions
                            func.operation(keyword);
                        }
                    }

                }
            }
        }
        console.log(matches);
    }

    /**
    * Check for modifiers words in the dictionnary in the given key and sentence
    * @method checkModifiers
    * @param {Object} key a key from the dictionnary already known
    * @param {Object} sentence
    */
    checkModifiers(key, sentence){

        let matches = [];

        for(let t in sentence){

            let token = sentence[t];

            for(let i in this.modifiersWords){

                let keyword = this.modifiersWords[i];

                if(token.string.toLowerCase() === keyword){

                    matches.push(keyword);

                    let func = this.findOperationFromModifierWord(keyword, key);

                    console.log("found modifier", func);

                    //do we have a modifier for this effect ?
                    if(func.modifier.keyword === key){

                        console.log("calling modifier", func.operation);
                        //call the modifiers function
                        func.operation(token.string);
                    }
                }
            }
        }
        console.log(matches);
    }

    /**
    * Check for words in the dictionnary against the given token
    * @method checkKeyword
    * @param {Object} token
    */
    /*checkKeyword(token){

        this.matches = [];

        for(let i in this.words){

            let keyword = this.words[i];

            if(token.string.toLowerCase() === keyword){

                this.matches.push(keyword);
                let func = this.findOperationFromWord(keyword);
                if(func){
                    console.log("calling", func);
                    func();
                }
            }
        }
        console.log(this.matches);
    }*/

    /**
    * Creates a flat array from nested arrays of words
    * @method generateFlatKeywordsArray
    * @param {Array} words
    */
    generateFlatArray(words){

        //converts to a flat array
        let array = [];

        words.forEach( function(el){
            el.forEach( function(el){
                array.push(el);
            });
        });

        return array;
    }

    /**
    * Creates a regexp from all the keywords for live search in livecoding
    * @method generateRegExpFromKeywords
    * @param {Array} words
    */
    /*generateRegExpFromWords(words){

        this.generateFlatKeywordsArray(words);

        //then to a regexp to use it for live search
        let regExp = "/";

        for(var i in this.words){

            regExp += this.words[i];

            if(i < this.words.length-1){
                regExp += "|";
            }

        }

        regExp += "/g";

        return regExp;
    }*/

    /**
    * get the fonction bound in dict to this word {key: key, operation: function}
    * @method findOperationFromWord
    * @param {String} words
    * @return the corresponding function to call for the word
    */
    findOperationFromWord(word){

        let entries = Object.keys(this.keywords);

        for(let i in entries){

            let key = entries[i];
            let words = this.keywords[key];

            for(let j in words){

                let tempWord = words[j];

                if(word === tempWord){

                    return {key: key, operation: this.operations[key]};
                }
            }
        }
    }

    /**
    * get the fonction bound in dict to this modifier {modifier: modifier, operation: function};
    * @method findOperationFromModifierWord
    * @param {String} words
    * @return the corresponding function to call for the modifier
    */
    findOperationFromModifierWord(word, baseKey){

        /*
         info: this.modifiers = {
            red: {keyword: "print", words: redWords},
            green: {keyword: "print", words: greenWords}
        };*/

        let entries = Object.keys(this.modifiers);

        for(let i in entries){

            let key = entries[i];
            let modifier = this.modifiers[key];

            for(let w in modifier.words){

                if(word === modifier.words[w]){

                    let result = {modifier: modifier, operation: this.operations[key]};

                    //look for a special operation binding defined in the modifier
                    if(modifier.operation){
                        result.operation = this.operations[modifier.operation];
                    }

                    if(modifier.keyword === baseKey){
                        console.log(modifier.keyword, baseKey);
                        return result;
                    }
                }
            }
        }
    }

};