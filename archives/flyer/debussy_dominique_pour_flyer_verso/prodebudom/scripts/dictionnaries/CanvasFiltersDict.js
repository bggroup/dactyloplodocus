class CanvasFiltersDict extends BaseDict{

    constructor(canvasPrinter, DOMPrinter){

        super(canvasPrinter, DOMPrinter);

        this.filter = new CanvasFilters(canvasPrinter.canvas);

        let blurWords = ["flou", "flouter", "troubler"];
        let sharpenWords = ["net", "craquant"];
        let brightWords = ["clair", "éclairé", "pâle", "pale"];
        let darkWords = ["sombre", "ombré", "noir"];

        this.keywords = {
            blur: blurWords,
            sharpen: sharpenWords,
            bright: brightWords,
            dark: darkWords,
        };

        this.modifiers = {
            //blur: {keyword: "blur", words: redWords}
        };

        this.operations = {
            blur: this.blur.bind(this),
            sharpen: this.sharpen.bind(this),
            bright: this.bright.bind(this, 1),
            dark: this.bright.bind(this, -1),
        };

        super.processWordsAndModifiers();
    }

    blur(){
        
        this.filter.blur(2);
    }

    sharpen(){
        
        this.filter.sharpen(10);
    }
    
    bright(val){
        
        this.filter.brightness(10*val);
    }

};
