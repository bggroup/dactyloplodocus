class FauneDict extends BaseDict{

    constructor(canvasPrinter, DOMPrinter, audioInputDict, liveCodingTextArea){

        super(canvasPrinter, DOMPrinter);

        this.audioInputDict = audioInputDict;
        this.liveCodingTextArea = liveCodingTextArea;
        this.textArea = liveCodingTextArea.textArea;

        //register this objects callback in the peak manager of audio input dict.
        this.audioInputDict.addPeakCallback(this.faune.bind(this));

        let fauneWords = ["faune"];
        let startWords = ["s'écrit", "s'inscrit"];
        let stopWords = ["s'arrête"];

        this.stringIndex = 0;
        this.fauneIndex = 0;

        this.keywords = {
            faune: fauneWords,
        };

        this.modifiers = {
            fauneOn: {keyword: "faune", operation: "start", words: startWords},
            fauneOff: {keyword: "faune", operation: "stop", words: stopWords}
        };

        this.operations = {
            faune: this.faune.bind(this),
            start: this.start.bind(this),
            stop: this.stop.bind(this)
        };

        super.processWordsAndModifiers();

        //some special controlers for keyboard
        window.addEventListener("keydown", (event) => {

            if(event.code === "Enter" && event.ctrlKey){
                this.stop();
            }

        });
    }

    start(){

        //active the mic listening
        this.audioInputDict.listen(true);
        this.active = true;
    }

    stop(){
        this.active = false;
    }

    faune(val){

        if(this.active){

            let str = faune[this.fauneIndex][this.stringIndex];
            this.stringIndex++;

            if(this.stringIndex > faune[this.fauneIndex].length-1){
                this.stringIndex = 0;

                this.fauneIndex++;
                this.fauneIndex %= faune.length;

            }

            this.textArea.value += str;
        }

    }

};