class LiveTextDict extends BaseDict{

    constructor(liveCodingTextArea){

        super();

        this.liveCodingTextArea = liveCodingTextArea;
        this.textArea = liveCodingTextArea.textArea;

        let textWords = ["écriture", "texte", "écrit"];
        let sizeUpWords = ["gros", "grand", "agrandi", "s'agrandi"];
        let sizeDownWords = ["petit", "réduit"];

        this.keywords = {
            text: textWords,
        };

        this.modifiers = {
            colors: {keyword: "text", operation: "colors", words: this.colorsWords},
            sizeUp: {keyword: "text", operation: "sizeUp", words: sizeUpWords},
            sizeDown: {keyword: "text", operation: "sizeDown", words: sizeDownWords},
        };

        this.operations = {
            colors: this.setColor.bind(this),
            sizeUp: this.setSize.bind(this, 1),
            sizeDown: this.setSize.bind(this, -1)
        };

        super.processWordsAndModifiers();
    }

    /**
     * val is given by the super class as an argument
     */
    setColor(val){
        console.log(val);
        this.textArea.style.color = this.colorDictionnary[val];
    }

    setSize(val){

        var fontSizeString = getComputedStyle(this.textArea)["font-size"];
        var fontSize = Number( fontSizeString.substring(0, fontSizeString.length-2) );

        if(fontSize > 5){
            this.textArea.style.fontSize = (fontSize + 5 * val) + "px";
        }else{
            this.textArea.style.fontSize = "5px";
        }
        //this.liveCodingTextArea.computeGlyphSize(this.textArea.style.fontSize);
    }

};