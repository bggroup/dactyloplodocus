const CANVAS = 0;
const DOM = 1;

class PrintersDict extends BaseDict{

    constructor(canvasPrinter, DOMPrinter){

        super(canvasPrinter, DOMPrinter);

        this.repeatable = true;

        this.currentPrinter = this.canvasPrinter;

        let printWords = ["imprime", "imprimer", "impression", "imprimé", "imprima", "imprimera"];
        let swicthToCanvasWords = ["pixels", "peinture", "couches", "couche", "canvas", "toile"];
        let swicthToDOMWords = ["morceaux", "morceau", "cartouches", "étiquettes", "étiquette"];
        let eraseWords = ["effacer", "efface", "vide", "vider"];

        
        this.keywords = {
            print: printWords,
            switchToCanvas: swicthToCanvasWords,
            swicthToDOM: swicthToDOMWords,
            erase: eraseWords,
        };

        this.modifiers = {
            colors: {keyword: "print", words: this.colorsWords},
        };

        this.operations = {
            print: this.print.bind(this),
            switchToCanvas: this.switchToPrinter.bind(this, CANVAS),
            swicthToDOM: this.switchToPrinter.bind(this, DOM),
            erase: this.erase.bind(this),
            colors: this.setColor.bind(this)
        };

        super.processWordsAndModifiers();
    }

    /**
     * val is given by the super class as an argument
     */
    setColor(val){
        console.log(val);
        this.currentPrinter.color = this.colorDictionnary[val];
    }

    switchToPrinter(val){

        console.log(val);

        switch(val){

            case CANVAS :
                this.currentPrinter = this.canvasPrinter;
                break;
            case DOM :
                this.currentPrinter = this.DOMPrinter;
                break;
        }
    }

    print(){

        console.log("this.currentPrinter", this.currentPrinter);
        this.currentPrinter.print();
    }

    erase(){
        console.log("this.currentPrinter", this.currentPrinter);
        this.currentPrinter.erase();
    }

};