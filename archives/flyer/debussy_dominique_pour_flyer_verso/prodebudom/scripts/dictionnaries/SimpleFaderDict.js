class SimpleFaderDict extends BaseDict{

    constructor(canvasPrinter, DOMPrinter){

        super(canvasPrinter, DOMPrinter);

        this.transparency = 0;
        this.transparencySpeed = .001;
        this.timer;

        let fadeWords = ["disparaître", "évaporer"];

        this.keywords = {
            fade: fadeWords,
        };

        this.modifiers = {
            //red: {keyword: "print", words: redWords}
        };

        this.operations = {
            fade: this.fade.bind(this)
        };

        super.processWordsAndModifiers();
    }

    fade(val){

        if(!this.timer){
            this.timer = setInterval(this.update.bind(this), this.fps);
        }
    }

    update(){

        this.transparency += this.transparencySpeed;

        this.canvasPrinter.canvasContext.fillStyle = "rgba(255,255,255,"+this.transparency+")";
        this.canvasPrinter.canvasContext.fillRect(0,0, this.canvasPrinter.canvas.width, this.canvasPrinter.canvas.height)

        if(this.transparency >= 1){
            clearInterval(this.timer);
            this.timer = undefined;
        }

    }

};