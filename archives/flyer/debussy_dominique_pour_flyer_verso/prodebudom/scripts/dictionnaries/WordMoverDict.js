class WordMoverDict extends BaseDict{

    constructor(canvasPrinter, DOMPrinter){

        super(canvasPrinter, DOMPrinter);

        this.timer;
        this.on = false;
        this.fps = 1000/20;

        this.rotateSpeed = 0;
        this.giggleSpeed = 0;
        this.speedFactor = .01;

        let rotateWords = ["tournoyer", "pivoter", "tourner"];
        let giggleWords = ["vibrer", "vibre", "trembler", "tremble"];

        let stopWords = ["arrêter", "stopper", "finir", "fini"];

        let speedUpWords = ["vite", "rapidement"];
        let speedDownWords = ["lentement", "doucement"];

        this.keywords = {
            rotate: rotateWords,
            giggle: giggleWords,
            stop: stopWords,
        };

        this.modifiers = {
            rotateUp: {keyword: "rotate", operation: "rotateUp", words: speedUpWords},
            rotateDown: {keyword: "rotate", operation: "rotateDown", words: speedDownWords},
            giggleUp: {keyword: "giggle", operation: "giggleUp", words: speedUpWords},
            giggleDown: {keyword: "giggle", operation: "giggleDown", words: speedDownWords}
        };

        this.operations = {
            rotate: this.rotate.bind(this),
            giggle: this.giggle.bind(this),
            stop: this.stop.bind(this),
            rotateUp: this.rotateSpeedChange.bind(this, 1),
            rotateDown: this.rotateSpeedChange.bind(this, -1),
            giggleUp: this.giggleSpeedChange.bind(this, 1),
            giggleDown: this.giggleSpeedChange.bind(this, -1),
        };

        super.processWordsAndModifiers();
    }

    rotate(){

        if(!this.timer){
            this.timer = setInterval( this.update.bind(this), this.fps);
        }
        if(this.rotateSpeed === 0){
            this.rotateSpeed = .01;
        }

        this.on = true;
    }

    giggle(){

        if(!this.timer){
            this.timer = setInterval( this.update.bind(this), this.fps);
        }
        if(this.giggleSpeed === 0){
            this.giggleSpeed = 1;
        }

        this.on = true;
    }


    stop(){

        this.on = false;

        if(this.timer){
            clearInterval(this.timer);
            this.timer = undefined;
        }

        this.rotateSpeed = 0;
        this.giggleSpeed = 0;
    }

    rotateSpeedChange(val){

        this.rotateSpeed += this.speedFactor * val;

        if(this.rotateSpeed < 0){
            this.rotateSpeed = 0;
        }

        console.log("this.rotateSpeed", this.rotateSpeed);
    }

    giggleSpeedChange(val){

        this.giggleSpeed += (this.speedFactor*10) * val;
        console.log("this.giggleSpeed", this.giggleSpeed);

    }

    update(){

        if(this.on){

            let elements = this.DOMPrinter.htmlElements;

            for(let i in elements){

                elements[i].transform.rotation.z += this.rotateSpeed;

                elements[i].transform.position.x += this.giggleSpeed * randomFromTo(0, 1);
                elements[i].transform.position.y += this.giggleSpeed * randomFromTo(0, 1);
                elements[i].transform.position.z += this.giggleSpeed * randomFromTo(0, 1);

                elements[i].transform.apply();
            }

        }

    }

}
