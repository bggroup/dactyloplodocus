class Lexer{

    constructor(tokenizer){
        
        //get what we need from the tokenizer
        this.tokens = tokenizer.tokens;
        this.punctuations = tokenizer.punctuations;

        //we'll use array for sentences, we must copy tokens ref to if
        this.tokensArray = [];

        //separators for sentences
        this.separators = [".", "?", "!"];

        //TODO ajouter des tags de type (adj, verb, noun)
        var adjList = [];
        var verbList = [];
        var nounList = [];

        var numberRegExp = /[0-9]+/;
        var punctuationRegExp = /[.,?!;]/;

        var keys = Object.keys(this.tokens);

        for(var i=0; i<keys.length; i++){

            var key = keys[i];
            var token = this.tokens[key];

            if( token.string.match(numberRegExp) && !token.string.match(/\D/)){
                this.tokens[key].type = "number";
            }else if( token.string.match(punctuationRegExp) ){
                this.tokens[key].type = "punctuation";
            }else{
                this.tokens[key].type = undefined;
            }

            this.tokensArray.push(this.tokens[key]);
        }

        this.sentences = this.generateSentences(this.tokensArray);
        //TODO : repérer les , : "" () etc.

        //console.log(this.sentences);
    }

    /**
    * generates an array of tokens that represents a sentence
    * @method generateSentences
    * @param {Object} tokens
    * @return {Array} description
    */
    generateSentences(tokens){

        var sentences = [];
        var tokens = tokens || this.tokensArray;

        //construct a rexexp for punctuation with a string (helpful to adapte punct. list)
        var regexString = "[";
        for(var p in this.separators){

            var punct = this.separators[p];
            regexString += punct + "|";
        }
        regexString += "]";

        var regex = new RegExp(regexString, "g");

        var startIndex = 0;
        var endIndex = 0;

        for(var i=0; i < tokens.length; i++){

            var token = tokens[i];

            if(token.string.match(regex)){

                endIndex = i + 1;
                var sentence = tokens.slice(startIndex, endIndex);
                startIndex = endIndex;

                if(sentence.length > 0){
                    sentences.push(sentence);
                }
            }
        }
        return sentences;
    };

    
    findSentenceFromToken(token){

        var result;

        if(!token){
            return;
        }

        for(var s in this.sentences){

            var sentence = this.sentences[s];

            for(var t in sentence){

                var tok = sentence[t];

                if(token === tok){

                    result = sentence;
                    break;
                }
            }
        }

        return result;
    }
}