var runner;

//create the textarea for live coding
window.addEventListener("load", function(){

    //get the config file and then launch the runner
    loadJSON("scripts/config.json", setup);

});

function setup(config){

    console.log("config", config);

    var textArea = document.getElementById("liveCodingTextArea");
    runner = new Runner({textArea: textArea, config: config});
}

class Runner{

    constructor(params){

        this.config = params.config || {};
        this.textArea = params.textArea;

        //Live Coding
        this.liveCodingTextArea = new LiveCodingTextArea({textArea: this.textArea,
                                                          inputCallback: this.inputChanged.bind(this),
                                                          caretPositionCallback : this.caretPositionChanged.bind(this)});

        this.tokenizer;
        this.lexer;
        this.sementizer = new Sementizer();//singleton

        //see dictionnaries at the end of constructor

        this.currentToken;
        this.carretPosition;


        //GRAPHICS

        //canvas printer
        this.canvasPrinter = new CanvasPrinter({textArea: this.textArea});
        //we need to pass a ref to the live coding text writting frame for font & line inner computing
        this.canvasPrinter.setLiveCodingRef(this.liveCodingTextArea);

        //Dom printer
        this.domPrinter = new DOMPrinter({textArea: this.textArea});
        this.domPrinter.setLiveCodingRef(this.liveCodingTextArea);

        //Adds dictionnaries
        this.sementizer.addDictionnary(new PrintersDict(this.canvasPrinter, this.domPrinter));
        this.sementizer.addDictionnary(new WordMoverDict(this.canvasPrinter, this.domPrinter));
        this.sementizer.addDictionnary(new SimpleFaderDict(this.canvasPrinter, this.domPrinter));
        this.sementizer.addDictionnary(new LiveTextDict(this.liveCodingTextArea));
        this.sementizer.addDictionnary(new CanvasFiltersDict(this.canvasPrinter, this.domPrinter));
        //special case for audio, we add some params after instanciation
        let audioInput = new AudioInputDict(this.canvasPrinter, this.domPrinter);
        audioInput.setAudioInput("live");
        audioInput.peakLimit = this.config.peakLimit || 80;
        audioInput.peakTemporizer = 1000 / this.config.peakTemporizer || 1000/50;
        
        this.sementizer.addDictionnary(audioInput);
        this.sementizer.addDictionnary(new FauneDict(this.canvasPrinter, this.domPrinter, audioInput ,this.liveCodingTextArea));

        this.debug = true;
    }

    /**
    * we get the textarea value from the liveCoding object (via inputCallback)
    * @method inputChanged
    * @param {Object} string
    */
    inputChanged(string){

        var text = string;

        this.tokenizer = new Tokenizer(string);
        this.lexer = new Lexer(this.tokenizer);

        this.currentToken = this.tokenizer.findTokenFromIndex(this.carretPosition);
        this.currentSentence = this.lexer.findSentenceFromToken(this.currentToken);

        if(this.currentSentence){
            this.sementizer.updateSentences(this.lexer.sentences);
            //console.log("inputChanged");
        }

    };

    /**
    * Description for caretPositionChanged
    * @method caretPositionChanged
    * @param {Object} pos$
    */
    caretPositionChanged(pos){

        this.carretPosition = pos;

        if(this.tokenizer){

            this.currentToken = this.tokenizer.findTokenFromIndex(this.carretPosition);
            this.currentSentence = this.lexer.findSentenceFromToken(this.currentToken);

        }
    };

    refreshConfig(json){
        
        if(json){
            
            this.config = json;
            console.log("updated config", this.config);
            
            //transfers it to the dictionnaries that uses configs
            let audioInputs = this.sementizer.findDictionnaryInstances(AudioInputDict);
            
            if(audioInputs){
                
                audioInputs.forEach( (dict) =>{
                    dict.peakLimit = this.config.peakLimit;
                    dict.peakTemporizer =  1000 / this.config.peakTemporizer;
                })
                
            }
            
            
        }else{
            
            loadJSON("scripts/config.json", this.refreshConfig.bind(this));
        }
    }

}