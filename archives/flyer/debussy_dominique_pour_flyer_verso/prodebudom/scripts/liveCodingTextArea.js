/**
 * Main liveCoding textarea used to show code
 * @class LiveCodingTextArea
 * @param params parameters obj
 * @param params.fluxusEffect
 * @param params.textArea
 * @param params.inputCallback
 */
class LiveCodingTextArea{

    constructor(params){

        //html element ref
        this.textArea = params.textArea || this.createTextArea();
        //callback to be called when the text input changed
        this.inputCallback = params.inputCallback;

        //callback for input events
        this.textArea.addEventListener("input", this.textAreaInputChanged.bind(this));
        this.textArea.addEventListener("select", this.textAreaSelectChanged.bind(this));
        this.textArea.addEventListener("keyup", this.textAreaArrowKeys.bind(this));
        this.textArea.addEventListener("mouseup", this.textAreaMouseUp.bind(this));
        window.addEventListener("resize", this.textAreaResized.bind(this));

        //get font size from css
        var tempFontSize = getComputedStyle(this.textArea)["font-size"];
        this.currentFontSize = Number(tempFontSize.substring(0, tempFontSize.length-2));

        //for fluxus like effect
        this.fluxusEffect = params.fluxusEffect || false; //to cut off fluxus effect

        this.maxFontSize = this.currentFontSize;
        this.minFontSize = params.minFontSize || 40;
        this.fontSizeFactor = 1.5;

        this.padding = 2;
        
        this.lineLength = 0;
        this.lineLengthMemory = 0;
        this.textLengthMemory = 0;
        this.maxLine = 0;
        this.maxChars = 0;

        this.caretPosition = 0;
        this.caretPositionCallback = params.caretPositionCallback;

        this.glyphBox = document.createElement("span");
        document.body.appendChild(this.glyphBox);
        this.glyphBox.classList.add("liveCodingFont");
        this.glyphBox.style.position = "absolute";
        this.glyphBox.style.top = "0px";
        this.glyphBox.style.left = "0px";
        this.glyphBox.innerHTML = "_";
        this.glyphBox.style.fontSize = this.currentFontSize + "px";
        /*this.glyphBox.style.transition = "none";
        this.glyphBox.style.webkitTransition = "none";*/

        this.computeGlyphSize(this.currentFontSize);

        //to avoid strange bug of size not updated, regular update (but low freq)
        if( this.fluxusEffect){
            this.updateInterval = setInterval(this.updateFontSize.bind(this), 500);
        }
    }

    /**
    * Input changed (via keyboard)
    */
    textAreaInputChanged(e){

        //update caret position in string
        this.caretPosition = this.textArea.selectionEnd;

        //call the given callback for custom string processing
        if(this.inputCallback){
            this.inputCallback(this.textArea.value);
        }
        if(this.caretPositionCallback){
            this.caretPositionCallback(this.caretPosition);
        }

        //visual effect
        if( this.fluxusEffect){
            this.updateFontSize();
        }
        this.textLengthMemory = this.textArea.textLength;
    };

    /**
    * Selection changed
    */
    textAreaSelectChanged(e){

        this.caretPosition = this.textArea.selectionEnd;
        if(this.caretPositionCallback){
            this.caretPositionCallback(this.caretPosition);
        }
    };

    /**
    * Arrow keys detection to update caret position
    */
    textAreaArrowKeys(e){

        var codeToCheck = ["ArrowLeft", "ArrowRight", "ArrowUp", "ArrowDown"];

        for(var i in codeToCheck){

            var keyCode = e.code || e.key;

            //update the caret position as we found an arrow key call
            if(codeToCheck[i] === keyCode){
                this.caretPosition = this.textArea.selectionEnd;
                if(this.caretPositionCallback){
                    this.caretPositionCallback(this.caretPosition);
                }
            }

        }
    };

    textAreaMouseUp(e){

        this.caretPosition = this.textArea.selectionEnd || 0;
        if(this.caretPositionCallback){
            this.caretPositionCallback(this.caretPosition);
        }
    }

    /**
    * Window resized event : recompute text font size!
    */
    textAreaResized(e){
        if( this.fluxusEffect){
            this.updateFontSize();
        }
    };

    /**
    * (fluxus) like font size effect
    */
    updateFontSize(){

        //we need a size!
        if(this.glyphSize){

            //less char (after erase!) = bigger
            if(this.textArea.textLength < this.textLengthMemory){

                if(this.textArea.textLength < this.lineLength/this.fontSizeFactor){

                    this.currentFontSize *= this.fontSizeFactor;
                }

                //more chars = smaller
            }

            if(this.textArea.textLength > this.lineLength){

                this.currentFontSize /= this.fontSizeFactor;
            }

            //size limits management
            if(this.currentFontSize < this.minFontSize){

                this.currentFontSize = this.minFontSize;
            }
            if(this.currentFontSize > this.maxFontSize){

                this.currentFontSize = this.maxFontSize;
            }

            //apply to css
            this.textArea.style.fontSize = this.currentFontSize + "px";
            //recompute glyph size and line props
            this.computeGlyphSize(this.currentFontSize);
        }
    }

    /**
    * for glyph approximate size for (fluxus) font-size effect
    * Works well with a monospace typeface!!
    */
    computeGlyphSize(fontSize){

        //make a span element if not already there
        if(!this.glyphBox){

            this.glyphBox = document.createElement("span");
            this.glyphBox.classList.add("liveCodingFont");
            this.glyphBox.style.position = "absolute";
            this.glyphBox.style.top = "0px";
            this.glyphBox.style.left = "0px";
            this.glyphBox.innerHTML = "_";
        }

        //comptute the size with a trick
        //if(fontSize >= this.minFontSize){
        document.body.appendChild(this.glyphBox);
        this.glyphBox.style.fontSize = fontSize + "px";

        this.glyphSize = {width: this.glyphBox.offsetWidth, height: this.glyphBox.offsetHeight};

        //the length in index, like for strings, the nb of char that can fit in a line
        this.lineLengthMemory = this.lineLength;//memorize old val
        this.lineLength = Math.floor((this.textArea.offsetWidth - (this.padding*2)) / this.glyphSize.width);

        this.maxLine = (this.textArea.offsetHeight - (this.padding*2)) / this.glyphSize.height;
        this.maxChars = Math.floor(this.lineLength) * Math.floor(this.maxLine);

        //this.textArea.maxLength = this.maxChars;
        
        //console.log("this.glyphSize", this.glyphSize, fontSize,  this.maxChars);
        
        //the width in px
        this.lineWidth = (this.textArea.offsetWidth / this.glyphSize.width)*this.glyphSize.width;
        document.body.removeChild(this.glyphBox);
        //}
        return this.glyphSize;
    };

}