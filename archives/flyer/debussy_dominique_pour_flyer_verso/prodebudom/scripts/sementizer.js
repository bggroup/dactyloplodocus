class Sementizer{

    constructor(){

        //a sentence is a list of tokens
        //tokens are objects as {string:"", startIndex:Number, endIndex:Number, type:String};
        this.sentences = [];
        this.dictionnaries = [];
    }

    /*
     * Adds a dictionnary instance to this sementizer
     */
    addDictionnary(val){
        this.dictionnaries.push(val);
    }
    
    /**
     * looks for a specific instance type of dictionnary
     * @return {Array} results
     */
    findDictionnaryInstances(type){
     
        let results = [];
        
        for(let i in this.dictionnaries){
            
            let dict = this.dictionnaries[i];
            
            if(dict instanceof type){
                results.push(dict);
            }
        }
        
        return results;
    }

    /*
     * find the keywords in a sentence and execute the binded fonctions in dictionnaries
     */
    executeDictionnarieSentence(sentence){

        for(let d in this.dictionnaries){

            let dict = this.dictionnaries[d];
            dict.checkKeywords(sentence);

        }
    }

    /*
     * find the token's binded fonctions in dictionnaries
     */
    executeDictionnariesToken(token){

        for(let d in this.dictionnaries){

            let dict = this.dictionnaries[d];
            dict.checkKeyword(token);

        }
    }

    /*
    * Updates all the sentences that the current Sementizer have from a new Lexer sentences
    * Loops in all sentences to detect changes. See updateSentence for sentence update implementation
    */
    updateSentences(sentences){

        //local sentence is empty, simply added the new ones from index 0
        if(this.sentences.length === 0){
            console.log("local sentence is empty, simply added the new ones");
            for(let i in sentences){
                this.sentences.push(sentences[i]);
                //all is new, executes all the dictionnaries
                this.executeDictionnarieSentence(sentences[i]);
            }

        }else{
            console.log("local sentences to be updated with",sentences);

            //add the new lines by complete replacement
            if(this.sentences.length !== sentences.length){

                //new lines added!
                if(this.sentences.length < sentences.length){

                    let index = this.sentences.length;
                    
                    for(let i=index; i<sentences.length; i++){
                        this.sentences.push(sentences[i]);
                        this.executeDictionnarieSentence(sentences[i]);
                    }
                    
                }else{
                    //something was erased, easy dirty = replace everything!
                    this.sentences = [];
                    for(let i in sentences){
                        this.sentences.push(sentences[i]);
                        //all is new, executes all the dictionnaries
                        this.executeDictionnarieSentence(sentences[i]);
                    }
                }

            }else{
                //update the current local sentences
                for(let i=0; i<sentences.length; i++){

                    let sentence = sentences[i];

                    let changed = this.updateSentence(sentence, i);

                    //dictionaries execution is only on what has changed
                    if(changed.length > 0){

                        /*for(let i in changed){
                            let token = changed[i].token;
                            let index = changed[i].index;

                            this.executeDictionnariesToken(token);
                            break;
                        }*/
                        this.executeDictionnarieSentence(sentence);
                    }
                }
            }
        }
    }

    /*
    * Looks for the changes in a single sentence.
    */
    updateSentence(sentence, index){

        let newTokens = [];
        let oldSentence = this.sentences[index];

        //console.log("update",oldSentence, "with", sentence);

        if(oldSentence){

            if(oldSentence.length !== sentence.length){

                //console.log("a word has been erased or added in", oldSentence, "replace the sentence with", sentence);
                oldSentence = sentence;
                this.executeDictionnarieSentence(oldSentence);

            }else{

                for(var t in oldSentence){

                    //compare tokens one by one (indices are the same! sentences share the same token length)
                    //console.log("compare",oldSentence[t], sentence[t]);
                    var equals = this.tokensAreEquals(oldSentence[t], sentence[t]);

                    if(!equals){

                        var changed = {token: sentence[t], index: t};
                        //update local sentence with the new token
                        oldSentence[t] = sentence[t];
                        //we must update all the next tokens indices too...
                        this.updateTokensForSentence(t, oldSentence, sentence);

                        newTokens.push(changed);
                    }
                }
            }
        }
        //console.log("newTokens", newTokens);
        return newTokens;
    }

    /**
    * updates tokens from an index by copying tokens from a new sentence to the old one
    */
    updateTokensForSentence(index, sentence, newSentence){

        for(var i=index; i<sentence.length; i++){
            sentence[i] = newSentence[i];
        }
    }

    /**
    * compares tokens string equality
    */
    tokensAreEquals(token1, token2){

        if(token1.string !== token2.string){
            return false;
        }
        return true;
    }
}