/**
 * Tokenizer takes a string and atomize it according the natural langage structure.
 * It construct the final object of tokens step by step internally
 * 
 * tokens are objects as {string:"", startIndex:Number, endIndex:Number, type:String};
 * Type is given by Lexer
 * 
 * @method Tokenizer
 * @param {String} string the string to tokenize
 */
class Tokenizer{

    constructor(string){
        
        this.punctuations = [",", ".", ";", ":", "?", "!"];

        this.baseString = string;
        this.baseStringCharCount = string.length;

        this.tokensFromWhiteSpace = this.splitText(string);
        this.tokens = this.splitByPunctuation(this.tokensFromWhiteSpace);
    }

    /**
    * First step of analyse, split the input string according to a separator
    * separator default to " "
    * @method splitText
    * @param {String} string
    * @param {String} sep separator
    * @return {Object} the tokens object populated with indices
    */
    splitText(string, sep){

        var text = string.replace(/\n|\f|\r|\t/, " ");//replace new line by space for processing
        var separator = sep || " ";//use space as the default separator

        var tempArray = text.split(separator);
        var resultArray = [];
        var index = 0;

        //we need to get the index of all words in the "visual" string, to operate smooth updates
        for(var i in tempArray){

            var item = tempArray[i];

            var startIndex = index;
            var endIndex = index + item.length-1;//exclude separator

            var newItem = {string: item, startIndex: startIndex, endIndex: endIndex};
            resultArray.push(newItem);

            index += item.length+1;//include separator
        }

        return resultArray;
    }

    /**
    * Seperate punctuation from words and digits 
    * @method splitByPunctuation
    * @param {Object} tokens
    * @return {Object} the token object populated with indices
    */
    splitByPunctuation(tokens){

        var destObj = {};

        //construct a rexexp for punctuation with a string (helpful to adapte punct. list)
        var regexString = "[";
        for(var p in this.punctuations){

            var punct = this.punctuations[p];
            regexString += punct + "|";
        }
        regexString += "]";

        //make the regex objects
        var punctRegex =  new RegExp(regexString, "g");
        var wordRegRex = new RegExp("[A-zÀ-ÿ0-9\d']+", "g");

        //String match results gives [string, index: nb, input: string]
        for(var t in tokens){

            //original token
            var token = tokens[t];

            var match;

            //find punctuation
            while ( (match = punctRegex.exec(token.string)) !== null) {
                // This is necessary to avoid infinite loops with zero-width matches
                if (match.index === punctRegex.lastIndex) {
                    punctRegex.lastIndex++;
                }

                //construct new objects
                var newToken = {string: match[0]};
                newToken.startIndex = token.startIndex + match.index;
                newToken.endIndex = newToken.startIndex;

                //add it the the destination object
                destObj[newToken.startIndex] = (newToken);
            }

            //find words and digits
            while ( (match = wordRegRex.exec(token.string)) !== null) {
                // This is necessary to avoid infinite loops with zero-width matches
                if (match.index === wordRegRex.lastIndex) {
                    wordRegRex.lastIndex++;
                }

                //construct new objects
                var newToken = {string: match[0]};
                newToken.startIndex = token.startIndex + match.index;
                newToken.endIndex = newToken.startIndex + newToken.string.length-1;

                //add it the the destination object
                destObj[newToken.startIndex] = (newToken);
            }
        }

        return destObj;
    };

    /**
    * findTokenFromIndex
    * @method findTokenFromIndex
    * @param {Object} index
    * @return {Object} token
    */
    findTokenFromIndex(index){

        var result;

        //walk through the object
        for(var prop in this.tokens){

            if(index >= this.tokens[prop].startIndex && index <= this.tokens[prop].endIndex){
                result = this.tokens[prop];
                break;   
            }
        }

        return result;
    };

}