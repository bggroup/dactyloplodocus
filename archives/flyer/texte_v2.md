Traditionnellement, l'écriture d'un programme informatique repose sur la répétition de trois étapes : écriture du code – compilation et exécution – débogage (correction et réécriture).

Ce schéma implique une séparation temporelle entre l’activité d’écriture du programme et celle de l'observation de ses résultats.

En étant ainsi séparé des effets visuels et sonores qu'il engendre, le code source, matrice de l'œuvre, devient invisible et inaccessible au spectateur. 

Le live-coding est une forme de programmation informatique qui cherche à bousculer cet état de fait. 

Le principe fondamental du live-coding est que l’écriture d’une instruction amène immédiatement à son exécution, sans étape de compilation préalable. 

L’effet d’une ligne de code est donc "instantané" et permet l’observation de ses résultats en temps réel, ou, autrement dit, en live.

Parmi les initiateurs de ce mouvement, les musiciens de la scène électronique considèrent qu’il est nécessaire de permettre l’accès au programme mis en œuvre lors d’un concert pour générer une musique et/ou la performance visuelle qui l'accompagne. 

Il s’agit donc de rendre un peu plus transparentes les boites noires que sont les ordinateurs en écrivant le code source devant le public, pendant la représentation elle-même. 

<!--
L’hypothèse est que l’écriture progressive du code doublée de l’apparition immédiate des résultats visuels permettront aux spectateurs de mieux appréhender la relation entre le code et ce qu’il génère, puisque l’ensemble des étapes de la création de la scène graphique est rendu visible. 
-->

Nous proposons ici la mise en œuvre de deux environnement expérimentaux de live-coding.

L'un reprenant certaines hypothèses émergeant 

en langage naturel : des phrases écrites en français deviennent le code source d'une création graphique en live.

Parce que Claude Debussy était un artiste particulièrement sensible aux pratiques d'écriture (musicale, littéraire, poétique, picturale) et plutôt enclin à "bousculer" les paradigmes esthétiques établis, nous avons choisi de fonder nos créations visuelles principalement sur du texte qui, par l'enchaînement des mots, se transforment en compositions abstraites.

– Dominique Cunin (PhD), artiste, enseignant en médias numériques à 
l'Esad•Valence, chercheur à EnsadLab, laboratoire de l'Ecole 
Nationale Supérieure des Arts décoratifs de Paris.

– Maxime Bouton & Emile Greis.

<!--Les technologies employées pour ces projets reposent sur des 
standards ouverts et publics (DOM, HTML, Javascript).-->
Codes sources des projets disponibles aux adresses suivantes :

https://gitlab.com/bggroup/debussick