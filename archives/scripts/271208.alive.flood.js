////////////////////
// DEBUG

function test() {
 //   console.log("test");
 //   console.log(this);
}

////////////////////
// VARIABLES

// config
uw = 1;
uh = 1;
ux = 64;
uy = 64;
speed = 1;
colorDefault = "red";
vitesse=100;
// math
sw = ux * uw;
sh = uy * uh;

// elements
s = document.getElementById('s');


////////////////////
// INIT FUNCTIONS

// createGrid
function createGrid() {
 //   console.log('f: createGrid');
 ratio = 99/sh;


 s.style.width = sw*ratio+'vh';
 s.style.height = sh*ratio+'vh';;
 for (var i = 0; i < ux; i ++) {
    for (var j = 0; j < uy; j ++) {
        var u = document.createElement('div');
        u.classList.add('u');
        u.setAttribute('y', i); 
        u.setAttribute('x', j); 
        var rdm = Math.random();
        if(rdm>0.2){
            //u.textContent='A';
            u.style.color='blue';
        }else{
           //u.textContent='B';
            u.style.color='rgb(128,255,128)';
        }
        u.style.fontSize= uh*ratio+'vh';
        u.style.width = uw*ratio+'vh';
        u.style.height = uh*ratio+'vh';
        var rdm = Math.random();
        if(rdm>0.3){
         u.style.backgroundColor = "black";
     }else{
         u.style.backgroundColor = "red";
     }
     s.appendChild(u);
 }
}
}

// variables
function variables() {
    uArr = s.getElementsByClassName('u');
}

// eventListener
function eventListener() {
  //  console.log('f: eventListener');

  document.addEventListener('mousedown', mouseDown, false); 
  document.addEventListener('mouseup', mouseUp, false); 

  for (var i = 0; i < uArr.length; i ++ ) {
    uArr[i].addEventListener('click', function() {
        var u = this;
        data(u);
        ufill(u, colorDefault);  
    }, false);
    uArr[i].addEventListener('mouseover', function() {
        var u = this;
        if (mouseClick) {
            ufill(u, colorDefault);
        }}, false);
}
}

////////////////////
// MOUSE FUNCTIONS

mouseClick = false;

function mouseDown() {
    mouseClick = true;
  //  console.log(mouseClick);
}

function mouseUp() {
    mouseClick = false;
  //  console.log(mouseClick);
}

////////////////////
// U FUNCTIONS

// data
function data(u) {
    return {
        char : u.textContent,
        x : Number(u.getAttribute('x')),
        y : Number(u.getAttribute('y')), 
        color : u.style.backgroundColor
    }
}

function unit(data) {
//todo
}

////////////////////
// FILLING FUNCTION

// ufill
function ufill(u, colorNew) {
   // console.log('f: ufill');
   u.style.backgroundColor = colorNew;
}

function uChangeChar(u, newChar) {
   // console.log('f: ufill');
   u.textContent = newChar;
   u.style.color='white';
}


// ufloodFill4
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function floodFillChar(u, newChar) {
  var rdm = Math.random();
    await sleep(vitesse/rdm);
    var floodArg = [];
  //  console.log('f: floodFill4');
  var datas = data(u);
  var x = datas.x;
  var y = datas.y;
  var color = datas.color;
  var char = datas.char;

  if (char != newChar) {
   // console.log('YOUHOU');
   uChangeChar(u, newChar);

   var rx = x+1;
   var lx = x-1;
   var by = y+1;
   var ty = y-1;

   right = s.querySelector('[x="' + rx + '"][y="' + y +'"]');

   left = s.querySelector('[x="' + lx + '"][y="' + y +'"]');

   bottom = s.querySelector('[x="' + x + '"][y="' + by +'"]');

   tope = s.querySelector('[x="' + x + '"][y="' + ty +'"]');


   if (right) {
    floodArg.push(right);
}
if (left) {
    floodArg.push(left);
}
if (bottom) {
    floodArg.push(bottom);
}
if (tope) {
 floodArg.push(tope);
}

for(var i in floodArg){
  floodFillChar(floodArg[i],newChar);
}

}
return
}


async function ufloodFill4(u, what, colorNew) {
var rdm = Math.random();
    await sleep(vitesse/rdm);
    var floodArg=[];
  //  console.log('f: floodFill4');
  var datas = data(u);
  var x = datas.x;
  var y = datas.y;
  var color = datas.color;
  //console.log(color,what);
  if (color == what) {

    ufill(u, colorNew);

    var rx = x+1;
    var lx = x-1;
    var by = y+1;
    var ty = y-1;

    right = s.querySelector('[x="' + rx + '"][y="' + y +'"]');

    left = s.querySelector('[x="' + lx + '"][y="' + y +'"]');

    bottom = s.querySelector('[x="' + x + '"][y="' + by +'"]');

    tope = s.querySelector('[x="' + x + '"][y="' + ty +'"]');


    if (right) {
        floodArg.push(right);
    }
    if (left) {
        floodArg.push(left);
    }
    if (bottom) {
        floodArg.push(bottom);
    }
    if (tope) {
     floodArg.push(tope);
 }

 for(var i in floodArg){
   ufloodFill4(floodArg[i],what,colorNew);
}

}
return
}



window.onload = function() {
    createGrid();
    variables();
    eventListener();

    //test = s.querySelector('[x="0"][y="0"]');
    //ufloodFill4(test, "red");
    
    test = s.querySelector('[x="0"][y="0"]');
    //console.log(test);
    //floodFillChar(test, 'B');
   // ufloodFill4(test, "red");

};


