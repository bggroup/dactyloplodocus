////////////////////
// OBJET


var Space = function(options){
	define(this,options,{
		uw: 16,
		uh: 16,
		ux: 32,
		uy: 32,
		time: 10,
		colorDefault: 'red',
		display:document.createElement('main'),
		scale:[0,0,1],
		rot:[0,0]
	});
	this.setupDisplay();
}

Space.prototype.setupDisplay = function(){
	this.display.style.width = sw +'px';
	this.display.style.height = sh +'px';
}
var units =[];

var Unit = function(options){

	define(this,options,{
		id: Math.random(),
		x: 16,
		y: 16,
		w: 32,
		h: 32,
		color: 'red',
		dom:document.createElement('div'),
	});

	this.dom.js=this;
	this.dom.style.position='absolute';
	this.dom.id=this.id;
	units.push(this);
	this.update();

}

Unit.prototype.change = function(options){
	define(this,options,{});
	this.update();
}

Unit.prototype.update = function(options){
	var dom = document.getElementById(this.id);
	if(dom == undefined){
		document.body.appendChild(this.dom);
		this.update();
	}else{
		define(this.dom.style,options,{
			'margin-left': this.x+'px',
			'margin-top': this.y+'px',
			'width': this.w+'px',
			'height': this.h+'px',
			'background-color': this.color,
		});
	}
}

////////////////////
// DEBUG

function test() {
    console.log("test");
    console.log(this);
}

////////////////////
// VARIABLES

// config
uw = 16;
uh = 16;
ux = 32;
uy = 32;
time = 1;
colorDefault = "red";

// math
sw = ux * uw;
sh = uy * uh;

// elements
s = document.getElementById('s');

////////////////////
// INIT 

// createGrid
function createGrid() {
    s.style.width = sw+'px';
    s.style.height = sh+'px';
    for (var i = 0; i < ux; i ++) {
        for (var j = 0; j < uy; j ++) {
            var u = document.createElement('div');
            u.classList.add('u');
            u.setAttribute('y', i); 
            u.setAttribute('x', j); 
            u.style.width = uw+'px';
            u.style.height = uh+'px';
            u.style.backgroundColor = "yellow";
            s.appendChild(u);
        }
    }
}

// variables
function variables() {
	uArr = s.getElementsByClassName('u');
}

// eventListener
function eventListener() {
  document.addEventListener('mousedown', mouseDown, false); 
  document.addEventListener('mouseup', mouseUp, false); 

  for (var i = 0; i < uArr.length; i ++ ) {
    uArr[i].addEventListener('click', function() {
        var u = this;
        data(u);
        ufill(u, colorDefault);  
    }, false);
    uArr[i].addEventListener('mouseover', function() {
        var u = this;
        if (mouseClick) {
            ufill(u, colorDefault);
        }}, false);
    }
}

////////////////////
// MOUSE

mouseClick = false;

function mouseDown() {
    mouseClick = true;
}

function mouseUp() {
    mouseClick = false;
}

////////////////////
// U FUNCTIONS

// data
function data(u) {
    var x = Number(u.getAttribute('x')); 
    var y = Number(u.getAttribute('y')); 
    var color = u.style.backgroundColor;
    return {
      x : x,
      y : y, 
      color : color
    }
}

function unit(datas) {
//todo
// return all elements with those attributes (x,y,color)
// works with 1 or more datas
// example: - all elements with x=8
//          - all elements with x=8 AND y=2
}

function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

////////////////////
// FILLING FUNCTION

// ufill
function ufill(u, colorNew) {
   // console.log('f: ufill');
   u.style.backgroundColor = colorNew;
}

// ufloodFill4
async function ufloodFill4(u, colorNew) {
	await sleep(time);
	var floodArg = [];
	var datas = data(u);
	var x = datas.x;
	var y = datas.y;
	var color = datas.color;
	if (color != colorNew) {
		ufill(u, colorNew);
		var rx = x+1;
		var lx = x-1;
		var by = y+1;
		var ty = y-1;
		right = s.querySelector('[x="' + rx + '"][y="' + y +'"]');
		left = s.querySelector('[x="' + lx + '"][y="' + y +'"]');
		bottom = s.querySelector('[x="' + x + '"][y="' + by +'"]');
		tope = s.querySelector('[x="' + x + '"][y="' + ty +'"]');
		if (tope) {
			floodArg.push(tope);
		}
		if (right) {
			floodArg.push(right);
		}
		if (bottom) {
			floodArg.push(bottom);
		}
		if (left) {
			floodArg.push(left);
		}
		for(var i in floodArg) {
			ufloodFill4(floodArg[i],colorNew);
		}
		return
	}
}

async function ufloodFill4d2(u, colorNew) {
	await sleep(time);
	var floodArg = [];
	var datas = data(u);
	var x = datas.x;
	var y = datas.y;
	var color = datas.color;
	if (color != colorNew) {
		ufill(u, colorNew);
		var rx = x+2;
		var lx = x-2;
		var by = y+2;
		var ty = y-2;
		right = s.querySelector('[x="' + rx + '"][y="' + y +'"]');
		left = s.querySelector('[x="' + lx + '"][y="' + y +'"]');
		bottom = s.querySelector('[x="' + x + '"][y="' + by +'"]');
		tope = s.querySelector('[x="' + x + '"][y="' + ty +'"]');
		if (tope) {
			floodArg.push(tope);
		}
		if (right) {
			floodArg.push(right);
		}
		if (bottom) {
			floodArg.push(bottom);
		}
		if (left) {
			floodArg.push(left);
		}
		for(var i in floodArg) {
			ufloodFill4d2(floodArg[i],colorNew);
		}
		return
	}
}

async function ufloodFill4d(u, colorNew) {
	await sleep(time);
	var floodArg = [];
	var datas = data(u);
	var x = datas.x;
	var y = datas.y;
	var color = datas.color;
	if (color != colorNew) {
		ufill(u, colorNew);
		var rx = x+1;
		var lx = x-1;
		var by = y+1;
		var ty = y-1;
		tope_right = s.querySelector('[x="' + rx + '"][y="' + ty +'"]');
		tope_left = s.querySelector('[x="' + lx + '"][y="' + ty +'"]');
		bottom_right = s.querySelector('[x="' + rx + '"][y="' + by +'"]');
		bottom_left = s.querySelector('[x="' + lx + '"][y="' + by +'"]');
		if (tope_right) {
			floodArg.push(tope_right);
		}
		if (bottom_right) {
			floodArg.push(bottom_right);
		}
		if (tope_left) {
			floodArg.push(tope_left);
		}
		if (bottom_left) {
			floodArg.push(bottom_left);
		}
		for(var i in floodArg) {
			ufloodFill4d(floodArg[i],colorNew);
		}
		return
	}
}

// ufloodFill8
async function ufloodFill8(u, colorNew) {
	await sleep(time);
	var floodArg = [];
	var datas = data(u);
	var x = datas.x;
	var y = datas.y;
	var color = datas.color;
	if (color != colorNew) {
		ufill(u, colorNew);
		var rx = x+1;
		var lx = x-1;
		var by = y+1;
		var ty = y-1;
		right = s.querySelector('[x="' + rx + '"][y="' + y +'"]');
		left = s.querySelector('[x="' + lx + '"][y="' + y +'"]');
		bottom = s.querySelector('[x="' + x + '"][y="' + by +'"]');
		tope = s.querySelector('[x="' + x + '"][y="' + ty +'"]');
		tope_right = s.querySelector('[x="' + rx + '"][y="' + ty +'"]');
		tope_left = s.querySelector('[x="' + lx + '"][y="' + ty +'"]');
		bottom_right = s.querySelector('[x="' + rx + '"][y="' + by +'"]');
		bottom_left = s.querySelector('[x="' + lx + '"][y="' + by +'"]');
		if (tope_right) {
			floodArg.push(tope_right);
		}
		if (bottom_right) {
			floodArg.push(bottom_right);
		}
		if (tope_left) {
			floodArg.push(tope_left);
		}
		if (bottom_left) {
			floodArg.push(bottom_left);
		}
		if (tope) {
			floodArg.push(tope);
		}
		if (left) {
			floodArg.push(left);
		}
		if (bottom) {
			floodArg.push(bottom);
		}
		if (right) {
			floodArg.push(right);
		}
		for(var i in floodArg) {
			ufloodFill8(floodArg[i],colorNew);
		}
		return
	}
}




////////////////////
// SORTING FUNCTION


////////////////////
// WINDOW ONLOAD
window.onload = function() {
	console.log(units);
	//createGrid();
	//variables();
	//eventListener();

	//test = s.querySelector('[x="15"][y="15"]');
	//test.style.outline = "solid red 2px";
    //console.log(test);
    //ufloodFill4(test, "red");
};


