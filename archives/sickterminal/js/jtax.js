/* jTax */
/*
var style=document.createElement('style');

style.id=('style');

document.children[0].children[0].appendChild(style);

var styleSheet = style.sheet;
var rulesIndex=0;

var all=newCssRule('*');
all.margin = '0';
all.padding = '0';
all.position = 'absolute';

var html=newCssRule('html');
html.margin = '0';
html.padding = '0';
html.width = '100%';
html.height = '100%';
html.position = 'absolute';
html.overflow ='hidden';
html.fontFamily = 'arial';

var body=newCssRule('body');
body.width = '100%';
body.height = '100%';
//body.backgroundColor = 'rgb(16,16,16)';
body.zIndex = '-9999999';

function newCssRule(objetName){
	styleSheet.insertRule(objetName+" {}", 0);
	var output = styleSheet.cssRules[rulesIndex].style;
	rulesIndex++;
	return output;	
}
*/
var style=document.createElement('style');
document.children[0].children[0].appendChild(style);

var styleSheet = style.sheet;
var rulesIndex=0;

function newCssRule(objetName,CSS){
	var str='{'
	for(var i in CSS){
		str+=' '+i+':'+CSS[i]+'; ';
	}
	str+='}';
	console.log(str);
	styleSheet.insertRule(objetName+" "+str, 0);
	var output = styleSheet.cssRules[rulesIndex].style;
	rulesIndex++;
	return output;	
}

function find(what,where){
	
	for (var i in where){
		if( where[i] == what ){
			console.log(where,what);
			return true
		}
	}
	return false
}
function check(what,where,otherwise){

	if (where instanceof Object) {

		for (var i in where){
			if(i==what){
				return where[i];
			}
			if(where[i]==what){
				return i;
			}
		}
	}

	if (where instanceof Array) {
		var found = [];
		for (var i in where){
			found.push(check(what,where[i],otherwise));
		}
		return found;
	}

	if(otherwise!=undefined)
		return otherwise;
}

function define(objet,from,along){
	for(var i in along){
		objet[i]=check(i,from,along[i]);
	}
}

function updateCSS(what,style){
	//console.log(what);
	for(var i in style){
		what.style[i]=style[i];
	}
}

function updateAttributes(dom,options) {
	for(var i in options){
		dom.setAttribute(i,options[i]);
	}
}

function random(min, max) {
	return {
		float:Math.random() * (max - min) + min,
		int:parseInt(Math.random() * (max - min) + min,)
	}
}

function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

var cssColors=['tan','peru','fuchsia','aquamarine','cadetblue','violet','indigo','sienna','olive','steelblue','maroon','slategrey','indianred','tomato','mediumblue','royalblue','sandybrown','skyblue','rebeccapurple','springgreen','khaki','grey','firebrick','deeppink','salmon','blue','dimgray','mediumaquamarine','blueviolet','thistle','chartreuse','teal','coral','plum','forestgreen','lawngreen','navy','lime','purple','hotpink','midnightblue','pink','mediumseagreen','slategray','burlywood','olivedrab','greenyellow','mediumslateblue','magenta','mediumturquoise','snow','silver','azure','cornflowerblue','ivory','wheat','goldenrod','chocolate','slateblue','limegreen','red','moccasin','yellowgreen','green','mediumpurple','orange','deepskyblue','saddlebrown','dimgrey','gold','mediumspringgreen','rosybrown','crimson','orchid','peachpuff','mistyrose','powderblue','brown','cyan','dodgerblue','turquoise','mediumorchid','seagreen','mediumvioletred','gray','aqua','orangere','black'];