# version 1.0

## fonctions

- fill
  * backgroundColor
  * border
  * color
  * letter
- vLine
- hLine
- floodFill4 soft
- floodFill4 hard
- floodFill8 soft
- floodFill8 hard
- floodFill4 diagonale soft
- floodFill4 diagonale hard
- text
 * letter
