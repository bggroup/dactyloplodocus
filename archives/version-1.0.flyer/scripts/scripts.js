/*#Generic functions*/

function define(objet,from,along){
    for(var i in along){
        
        objet[i]=check(i,from,along[i]);
    }
}

function check(what,where,otherwise){

    if (where instanceof Object) {

        for (var i in where){
            if(i==what){

                return where[i];
            }
            if(where[i]==what){
                return i;
            }
        }
    }

    if (where instanceof Array) {
        var found = [];
        for (var i in where){
            found.push(check(what,where[i],otherwise));
        }
        return found;
    }

    if(otherwise!=undefined)
        return otherwise;
}

Keys = function(){

}

Keys.prototype.trigger = function(key,down){
    this[key]=down;	
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function random(min, max) {
	return {
		float:Math.random() * (max - min) + min,
		int:parseInt(Math.random() * (max - min) + min,)
	}
}

/*#Debussick variables*/

var speed=1;

/*#Debussick functions*/

/*##Matrix*/

Matrix = function(options){
    define(this,options,{
        lines:[],
        dom:document.createElement('table')
    });
}

Matrix.prototype.build = function(size){
    this.x=size[0];
    this.y=size[1];
    for(var y = 0 ; y < size[1] ; y++){

        var line = new Line({matrix:this,id:y});

        for(var x = 0 ; x < size[0] ; x++){

            var type = new Type({line:line,id:x});
            type.fill();
            line.types.push(type);
            line.dom.appendChild(type.dom);

        }

        this.lines.push(line);
        this.dom.appendChild(line.dom);

    }

}

/*##Line*/

Line = function(options){
    define(this,options,{
        id:undefined,
        types:[],
        matrix:undefined,
        dom:document.createElement('tr')
    });
}

/*##Type*/

Type = function(options){
    define(this,options,{
        id:undefined,
        shoulder:'transparent',
        face:' ',
        ink:[255,255,255],
        line:undefined,
        input:undefined,
        border:[
            [1,'transparent'],
            [1,'transparent'],
            [1,'transparent'],
                [1,'transparent'],
        ],
        dom:document.createElement('td')
    });
    this.dom.contentEditable = "true";
}

function update(dom,type){
    dom.textContent=type.face;
    dom.style.backgroundColor=type.shoulder;
}

Type.prototype.fill = function(options){
    define(this,options,this);
    if(this.input!=undefined){
        update(this.dom,this.input);
    }else{
        update(this.dom,this);
    }

}

Caret = function(options){
    define(this,options,{
        x:0,
        y:0,
        matrix:undefined,
        self:new Type({shoulder:'blue',face:'@'}),
        brush:new Type({shoulder:'black',face:' '}),
        keys:new Keys(),
    });
}

Caret.prototype.init = function(){
    var caret= this;
    document.onkeydown = function(e){
        console.log(caret.matrix);
        caret.keys.trigger(e.key,true);
        caret.draw();
        caret.move();
        caret.drop();

    }
    document.onkeyup = function(e){
        caret.keys.trigger(e.key,false);
    }
}

Caret.prototype.move = function(){

    this.matrix.lines[this.y].types[this.x].fill();

    if(this.keys.ArrowUp){
        this.y--;
    }

    if(this.keys.ArrowDown){
        this.y++;
    }

    if(this.keys.ArrowLeft){
        this.x--;
    }

    if(this.keys.ArrowRight){
        this.x++;
    }

    update(this.matrix.lines[this.y].types[this.x].dom,this.self);

}

Caret.prototype.drop = function(){
    if(this.keys['d']){
        this.matrix.lines[this.y].types[this.x].input=new Type({shoulder:'grey',face:' '});
    }
}

Caret.prototype.draw = function(){
    if(this.keys[' ']){
        var options = {shoulder:'blue'};
        this.brush.line=this.matrix.lines[this.y];
        this.brush.dom=this.matrix.lines[this.y].types[this.x].dom;
        this.brush.id=this.matrix.lines[this.y].types[this.x].id;
        this.brush.input=this.matrix.lines[this.y].types[this.x].input;
        console.log(this.brush);
        this.matrix.lines[this.y].types[this.x].fill(this.brush);
    }
}

async function floodfill4(type,color){

    await sleep(speed);

    if(type.shoulder!=color){

        type.fill({shoulder:color});

        var voisins=[];

        if(type.line.matrix.lines[type.line.id-1]){
            voisins.push(type.line.matrix.lines[type.line.id-1].types[type.id]);
        }

        if(type.line.matrix.lines[type.line.id+1]){
            voisins.push(type.line.matrix.lines[type.line.id+1].types[type.id]);
        }

        voisins.push(type.line.types[type.id-1]);
        voisins.push(type.line.types[type.id+1]);

        for(var i in voisins){
            if(voisins[i]){
                floodfill4(voisins[i],color);
            }
        }

    }
}

async function floodfill4s(type,newColor,oldColor){

    await sleep(speed);

    if(type.shoulder==oldColor){

        type.fill({shoulder:newColor});

        var voisins=[];

        if(type.line.matrix.lines[type.line.id-1]){
            voisins.push(type.line.matrix.lines[type.line.id-1].types[type.id]);
        }

        if(type.line.matrix.lines[type.line.id+1]){
            voisins.push(type.line.matrix.lines[type.line.id+1].types[type.id]);
        }

        voisins.push(type.line.types[type.id-1]);
        voisins.push(type.line.types[type.id+1]);

        for(var i in voisins){
            if(voisins[i]){
                floodfill4s(voisins[i],newColor,oldColor);
            }
        }
    }
}

/*##Text*/

async function text(type,string,color){ /*quelle est la couleur*/
    for (var i = 0; i < string.length; i++) {
        letter(type.line.types[type.id+i*8],string.charAt(i),color)
        await sleep(speed);
    }
}

async function letter(type,character,color) {
    var dec = character.charCodeAt(0);
    for (var i = 0; i < font8x8[dec].length; i ++) {
        for (var j = 0; j < font8x8[dec][i].length; j ++) {
            if (font8x8[dec][i][j] == 1) {
                type.line.matrix.lines[type.line.id+i].types[type.id+j].fill({shoulder:color});
                await sleep(speed);
            }
        }
    }
}


// # INIT //

matrix = new Matrix();
matrix.build([58,43]);
caret = new Caret({matrix:matrix,x:0,y:0});
caret.init();
caret.move();
document.body.appendChild(matrix.dom);




// carré test pour floodfill
//matrix.lines[1].types[1].fill({shoulder:'#FF0000',face:'A'});
//matrix.lines[2].types[1].fill({shoulder:'#FF0000',face:'A'});
//matrix.lines[3].types[1].fill({shoulder:'#FF0000',face:'A'});
//matrix.lines[4].types[1].fill({shoulder:'#FF0000',face:'A'});
//matrix.lines[5].types[1].fill({shoulder:'#FF0000',face:'A'});
//matrix.lines[6].types[1].fill({shoulder:'#FF0000',face:'A'});
//matrix.lines[1].types[6].fill({shoulder:'#FF0000',face:'A'});
//matrix.lines[2].types[6].fill({shoulder:'#FF0000',face:'A'});
//matrix.lines[3].types[6].fill({shoulder:'#FF0000',face:'A'});
//matrix.lines[4].types[6].fill({shoulder:'#FF0000',face:'A'});
//matrix.lines[5].types[6].fill({shoulder:'#FF0000',face:'A'});
//matrix.lines[6].types[6].fill({shoulder:'#FF0000',face:'A'});
//matrix.lines[1].types[1].fill({shoulder:'#FF0000',face:'A'});
//matrix.lines[1].types[2].fill({shoulder:'#FF0000',face:'A'});
//matrix.lines[1].types[3].fill({shoulder:'#FF0000',face:'A'});
//matrix.lines[1].types[4].fill({shoulder:'#FF0000',face:'A'});
//matrix.lines[1].types[5].fill({shoulder:'#FF0000',face:'A'});
//matrix.lines[1].types[6].fill({shoulder:'#FF0000',face:'A'});
//matrix.lines[6].types[1].fill({shoulder:'#FF0000',face:'A'});
//matrix.lines[6].types[2].fill({shoulder:'#FF0000',face:'A'});
//matrix.lines[6].types[3].fill({shoulder:'#FF0000',face:'A'});
//matrix.lines[6].types[4].fill({shoulder:'#FF0000',face:'A'});
//matrix.lines[6].types[5].fill({shoulder:'#FF0000',face:'A'});
//matrix.lines[6].types[6].fill({shoulder:'#FF0000',face:'A'});



// # TESTING //

testtype = matrix.lines[2].types[8];
testtype2 = matrix.lines[8].types[8];

//text(testtype2,'grocon','purple');
//text(testtype,'baba','purple');
//floodfill4(testtype,'purple');

async function test1() {
    for (var i = 0; i < 10; i ++) {
       floodfill4(testtype2,'#'+i+i+i+i+i+i);
       //await sleep(100);
    }
}

async function test2() {
    for (var i = 0; i < 10; i ++) {
        text(testtype,'Debussy','#'+i+i+i+i+i+i);
        await sleep(200);
    }
}

async function test3() {
    for (var i = 0; i < 10; i ++) {
        text(matrix.lines[i].types[i],'Debussy','#'+i+i+i+i+i+i);
        await sleep(200);
    }
}

async function test4() {
    for (var i = 0; i < 10; i ++) {
        floodfill4(matrix.lines[8].types[8+i*4],'#'+i+i+i+i+i+i);
        await sleep(1000);
        //await sleep(2000);
        //await sleep(100);
        // await sleep(200);
    }
}

async function test5() {
    for (var i = 0; i < 10; i ++) {
        var rdmx=random(0,matrix.x).int;
        var rdmy=random(0,matrix.y).int;
        floodfill4(matrix.lines[rdmy].types[random(0,matrix.x).int],cssColors[random(0,cssColors.length).int]);
        await sleep(1000);
        //await sleep(2000);
        //await sleep(100);
        // await sleep(200);
    }
}

//test1();
//test2();
//test3();
//test4();
//test5();



// # FLYER //

testtype = matrix.lines[2].types[8];
testtype2 = matrix.lines[8].types[8];

async function test1() {
    for (var i = 0; i < 10; i ++) {
       floodfill4(testtype2,'#FF00FF');
       //await sleep(1000);
       //await sleep(2000);
       //await sleep(100);matrix.lines[8].types[8]
    }
}
//test1();

//floodfill4(matrix.lines[2].types[2],'#FF00FF');
//floodfill4(matrix.lines[8].types[8],'#00FFFF');
//floodfill4(matrix.lines[16].types[16],'#FFFF00');
//


//async function pDebussy() {
//    for (var i = 0; i < 10; i ++) {
//        text(matrix.lines[10].types[i],'Debussy','#'+i+i+i+i+i+i);
//        await sleep(10);
//    }
//}
//
//async function pLive() {
//    for (var i = 0; i < 10; i ++) {
//        text(matrix.lines[i].types[20],'Live','#'+i+i+i+i+i+i);
//        await sleep(10);
//    }
//}
//
//async function p20() {
//    for (var i = 0; i < 10; i ++) {
//        text(matrix.lines[3+i].types[3],'20','#'+i+i+i+i+i+i);
//        await sleep(10);
//    }
//}
//
//async function p26() {
//    for (var i = 0; i < 10; i ++) {
//        text(matrix.lines[5+i].types[30-i],'26','#'+i+i+i+i+i+i);
//        await sleep(10);
//    }
//}
//
//async function pJanv() {
//    for (var i = 0; i < 10; i ++) {
//        text(matrix.lines[20].types[10+i],'Janv','#'+i+i+i+i+i+i);
//        await sleep(10);
//    }
//}
//
//async function pIer() {
//    for (var i = 0; i < 10; i ++) {
//        text(matrix.lines[30].types[10-i],'ier','#'+i+i+i+i+i+i);
//        await sleep(10);
//    }
//}
//
////floodfill4(matrix.lines[16].types[16],'#DDDDDD');
//
//pDebussy();
//pLive();
//p20();
//p26();
//pJanv();
//pIer();

//matrix.lines[1].types[1].shoulder:'black';

var cssColors=['tan','peru','fuchsia','aquamarine','cadetblue','violet','indigo','sienna','olive','steelblue','maroon','slategrey','indianred','tomato','mediumblue','royalblue','sandybrown','skyblue','rebeccapurple','springgreen','khaki','grey','firebrick','deeppink','salmon','blue','dimgray','mediumaquamarine','blueviolet','thistle','chartreuse','teal','coral','plum','forestgreen','lawngreen','navy','lime','purple','hotpink','midnightblue','pink','mediumseagreen','slategray','burlywood','olivedrab','greenyellow','mediumslateblue','magenta','mediumturquoise','snow','silver','azure','cornflowerblue','ivory','wheat','goldenrod','chocolate','slateblue','limegreen','red','moccasin','yellowgreen','green','mediumpurple','orange','deepskyblue','saddlebrown','dimgrey','gold','mediumspringgreen','rosybrown','crimson','orchid','peachpuff','mistyrose','powderblue','brown','cyan','dodgerblue','turquoise','mediumorchid','seagreen','mediumvioletred','gray','aqua','orangere','black'];

var cssGrey=[
    '#000000',
    '#111111',
    '#222222',
    '#333333',
    '#444444',
    '#555555',
    '#666666',
    '#777777',
    '#888888',
    '#999999',
    '#AAAAAA',
    '#BBBBBB',
    '#CCCCCC',
    '#DDDDDD',
    '#EEEEEE', 
    '#FFFFFF']

function rdm(min, max) {
	return {
		float:Math.random() * (max - min) + min,
		int:parseInt(Math.random() * (max - min) + min,)
	}
}

async function pFlyer() {
   
//for (var i = 0; i < 40; i ++) {
//   text(matrix.lines[rdm(0,matrix.y).int].types[rdm(0,matrix.x-20).int],'Debussy',cssGrey[rdm(3,10).int]);
//}
   
for (var i = 0; i < 40; i ++) {
  var rdmtype = matrix.lines[rdm(0,matrix.y).int].types[rdm(0,matrix.x).int];
  floodfill4s(rdmtype,cssGrey[rdm(4,11).int],rdmtype.shoulder);
}

//        text(matrix.lines[40].types[34],'Debussy','#'+i+i+i+i+i+i);
//        text(matrix.lines[40].types[24],'Debussy','#'+i+i+i+i+i+i);
//   
//   for (var i = 0; i < 10; i ++) {
//        text(matrix.lines[10+i].types[i],'Debussy','#'+i+i+i+i+i+i);
//        await sleep(10);
//    }
//    
//    for (var i = 0; i < 10; i ++) {
//        text(matrix.lines[i].types[20],'Live','#'+i+i+i+i+i+i);
//        await sleep(10);
//    }
//    
//    for (var i = 0; i < 10; i ++) {
//        text(matrix.lines[3+i].types[3],'20','#'+i+i+i+i+i+i);
//        await sleep(10);
//    }
//    
//    for (var i = 0; i < 10; i ++) {
//        text(matrix.lines[5+i].types[30-i],'26','#'+i+i+i+i+i+i);
//        await sleep(10);
//    }
//
//    for (var i = 0; i < 10; i ++) {
//        text(matrix.lines[20+i].types[10+i],'Janv','#'+i+i+i+i+i+i);
//        await sleep(10);
//    }
//    
//    await sleep(10000);
//    
//        text(matrix.lines[40].types[34],'Debussy','#'+i+i+i+i+i+i);
//    
//        for (var i = 0; i < 10; i ++) {
//        text(matrix.lines[30+i].types[10-i],'ier','#'+i+i+i+i+i+i);
//        await sleep(10);
//    }
//   
//   for (var i = 0; i < 10; i ++) {
//        text(matrix.lines[12+1].types[12+i],'Debussy','#'+i+i+i+i+i+i);
//        await sleep(10);
//    }

}


pFlyer();

//floodfill4s(matrix.lines[16].types[16],'#DDDDDD');


