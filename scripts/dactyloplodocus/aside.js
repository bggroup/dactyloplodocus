/*
                 ( aside.js )
               __ |/
              / _)
     _.----._/ /
    /         /
 __/ (  | (  |
/__.-'|_|--|_|

*/

// end crédits
function credits() {
    matrix.clear();
    floodfill({scope:'all',shoulder:'black'});
    cursor.log('€ Orchestre symphonique + Live coding€Samedi 20 janvier 20h00, Théâtre des Cordeliers (Romans)€Samedi 27 janvier 20h00, Église Sainte-Catherine (Valence)€€€Code sources:€ - gitlab.com/bggroup/dactyloplodocus€ - gitlab.com/bggroup/debussy€€');
}


// arrColor
function arrColor(combien) {
    var array = [];
    for (var i = 0; i < combien; i ++) {
     array.push(cssColors[random(0,cssColors.length).int]);
 }
 return array;
}

// randomFloodfill
function randomFloodfill(combien) {
    for (var i = 0; i < combien; i ++) {
        var rdmtype = matrix.lines[random(0,matrix.y).int].types[random(0,matrix.x).int];
        floodfill({type:rdmtype});
    }
}

// randomDrop
function randomDrop(combien) {
    var x=cursor.x;
    var y=cursor.y;
    for (var i = 0; i < combien; i ++) {
        var rdmline = random(0,matrix.y).int
        var rdmtype = random(0,matrix.x).int;
        //console.log(rdmline);
        //console.log(rdmtype);
        cursor.x = rdmtype;
        cursor.y = rdmline;
        cursor.events();
        cursor.drop();
    }
    cursor.x = x;
    cursor.y = y;
}

function diplo(text,reverse) {
    if(reverse){
        var text_r=''
        for(var i = text.length+1 ; i>0 ; i--){
            text_r+=text.charAt(i);
        }
        var tolog='€                ( '+text_r+' )€                 |\/€               __ €              / _)€     _.----._/ /  €    /         /   € __/ (  | (  |    €/__.-\'|_|--|_|    €';
        var tolog_r='';
        for(var i = tolog.length+1 ; i>0 ; i--){
           tolog_r+=tolog.charAt(i);
       }
       cursor.log(tolog_r);
   }else{
    cursor.log('€                ( '+text+' )€                 |\/€               __ €              / _)€     _.----._/ /  €    /         /   € __/ (  | (  |    €/__.-\'|_|--|_|    ');
}

}


function traffic(){
    cursor.y++;
    cursor.x=0;
    cursor.events();
    cursor.log('loop.event=\"cursor.reclaim();randomDrop(1);cursor.f()\"€loop.delay=100€this.pts=[[[0,1]],[[0,-1]],[[1,0]],[[-1,0]]]€this.rdmDir=function(){return parseInt(Math.random()*cursor.pts.length)}€loop.break()€floodfill({scope:\'all\',shoulder:\'black\'})€floodfill({scope:\'all\'})€this.f=function(){floodfill({dir:cursor.pts[cursor.rdmDir()]})}');
}

function weaver(){
     cursor.y++;
    cursor.x=0;
    cursor.events();
    cursor.log('loop.event=\"matrix.clean();cursor.reclaim();randomDrop(rdm()*4);flood();cursor.reclaim();\"€window.loop=newLoop()€window.flood=function(){floodfill({dir:pts()})}€window.pts=function(){return[[rdm(),-rdm()],[-rdm(),rdm()],[rdm(),rdm()],[-rdm(),-rdm()]]}€loop.auto=false€floodfill({scope:\'all\'})€window.rdm=function(){return parseInt(Math.random()*4)}€matrix.clean()€_floodfill.stop=true€_floodfill.stop=false%€randomDrop(5)');
}

function merci() {
    cursor.log('€                ( Merci )€                 |\/€               __ €              / _)€     _.----._/ /  €    /         /   € __/ (  | (  |    €/__.-\'|_|--|_|    ');
}

// rdm in arr
function rdmArr(arr) {
    var rdmEl = random(0,arr.length).int;
    return rdmEl;
}