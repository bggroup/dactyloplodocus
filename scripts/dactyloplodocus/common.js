/*
                 ( common.js )
               __ |/
              / _)
     _.----._/ /
    /         /
 __/ (  | (  |
/__.-'|_|--|_|

*/

var onkeydown_list=[];
function onkeyDown_f(e){
    for(var i in onkeydown_list){
        onkeydown_list[i](e);
    }
}

var colors=monet;

var faces = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';


function define(objet,from,along){
    if(!objet)objet={};
    if(!from)from={};
    for(var i in along){

        objet[i]=check(i,from,along[i]);
    }
    return objet;
}

function check(what,where,otherwise){

    if (where instanceof Object) {

        for (var i in where){
            if(i==what){
                if(where[i] || where[i]==0){
                    return where[i];
                }
            }
            if(where[i]==what){
                return i;
            }
        }
    }

    if (where instanceof Array) {
        var found = [];
        for (var i in where){
            found.push(check(what,where[i],otherwise));
        }
        return found;
    }

    if(otherwise!=undefined)
        return otherwise;
}

Keys = function(){}

Keys.prototype.trigger = function(key,down){
    this[key]=down;	
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function random(min, max) {
	return {
		float:Math.random() * (max - min) + min,
		int:parseInt(Math.random() * (max - min) + min)
	}
}

Loop = function(){
    this.stop=false;
    this.auto=true;
    this.key='F2';
    this.event="";
    this.delay=5000;
    this.play();
    var _loop =this;
    onkeydown_list.push(function(e){
        //console.log('manual_trigger',e);
        if(e.key==_loop.key && !_loop.auto){
            _loop.play();
        }
    })
    
}

Loop.prototype.break = function(){
    if(this.stop){
        this.stop=false;
        this.play();
    }else{
        this.stop=true;
    }
}

Loop.prototype.play = function(){
    if(this.stop)return
        if(typeof this.event == "string"){
            eval(this.event);
        }else{
            this.event();
        }
        var _loop=this;
        if(this.auto){
            setTimeout(function() {
                _loop.play();
            },this.delay);
        }
    }

    var loop = new Loop();