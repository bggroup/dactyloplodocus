/*
                 ( cursor.js )
               __ |/
              / _)
     _.----._/ /
    /         /
 __/ (  | (  |
/__.-'|_|--|_|

*/

Cursor = function(options){
    //console.log(options.display);
    define(this,options,{
        x:0,
        y:0,
        matrix:undefined,
        display:{shoulder:'white',face:'☺',ink:'black'},
        shoulder:undefined,
        face:undefined,
        ink:'rgba(255,255,255,0.65)',
        selectors:[],
        keys:new Keys(),
        commands:[],
        commands_index:0
    });
    //console.log(this.display);
}

window.Control=false;

Cursor.prototype.init = function(){

    var cursor= this;

    document.onkeydown = function(e){
        if(e.key=='Control'){
            window.Control=true
        }
        cursor.keys.trigger(e.key,true);
        cursor.events(e.key);
        onkeyDown_f(e);
    }

    document.onkeyup = function(e){
        if(e.key=='Control'){
            window.Control=false
        }
        cursor.keys.trigger(e.key,false);
    }
    cursor.events();
    this.blink({dom:this.selectors[0].dom});


}

Cursor.prototype.blink = function(options){
    var local = define(local,options,{
        time:750,
        dom:undefined,
        cursor:this,
        shoulder:'white',
        ink:'black',
        player:true,
    });
    if(!local.player){
        if(!local.cursor.display.output.input)return;
    }
    update(local.dom,local.cursor.display);
    setTimeout(function() {
        if(local.cursor.blink_stat){
            local.cursor.display.shoulder=local.shoulder;
            local.cursor.display.ink=local.ink;
            local.cursor.blink_stat=false;
            if(local.player)local.dom=local.cursor.selectors[0].dom;
            return local.cursor.blink(local);
        }else{
         local.cursor.display.shoulder=local.ink;
         local.cursor.display.ink=local.shoulder;
         local.cursor.blink_stat=true;
         if(local.player)local.dom=local.cursor.selectors[0].dom;
         return local.cursor.blink(local);
     }
 },local.time);
}

Cursor.prototype.log = async function(string,error,time){
    var _x=this.x;
    if(error){
        window.Control=true;
        var _s=this.shoulder;
        var _i=this.ink;
        this.shoulder='red';
        this.ink='cyan';
    }

    for(var i = 0 ; i<string.length ; i++){
        if(string.charAt(i)!='€'){
            this.write(string.charAt(i));

        }else{
            this.y++;
            this.x=_x
        }
        this.events();
        update(cursor.selectors[0].dom,cursor.selectors[0]);
        if(!time)time=17;
        await sleep(time);
    }
    if(error){
        window.Control=false;
        this.shoulder=_s;
        this.ink=_i;
    }
}

Cursor.prototype.write = function(key){

    if(key.length==1){

        this.face=key;
        this.matrix.lines[this.y].put_content(key,this.x,this.ink);
        
        if(this.x+1<this.matrix.x)this.x++;

    }else{

     if(key=='Backspace'){

        if(this.x-1>-1){

            this.matrix.lines[this.y].sub_content(this.x);
            var before=this.matrix.lines[this.y].get_content(this.x-1).before;

            if(parseInt(before[before.length-1])+1!=0){
                this.x=parseInt(before[before.length-1])+1;
            }else{
                this.x=this.matrix.x-1;
                if(this.y>0){
                    this.y--;
                    this.write('Backspace');
                    return
                }
                this.x=0;
            }

        }

    }

}

this.face=undefined;

}

Cursor.prototype.key_events = function(key){

    if(key=='/'){
        this.log(this.commands[this.commands_index]);
        if(this.commands_index>0)this.commands_index--;
    }
    if(key=='\\'){
        if(this.commands_index<this.commands.length-1)this.commands_index++;
        this.log(this.commands[this.commands_index]);
    }

    if(this.keys['#']){
        this.drop();
        cursor.keys.trigger('#',false);
        if(this.x+1<this.matrix.x)this.x++;
        this.selectors[0].fill(this);
    }

    if(this.keys['$']){
        this.reclaim();
        cursor.keys.trigger('$',false);
    }

    if(key=='%'){
        try{
            var command=this.matrix.lines[this.y].get_content().script;
            if(command!=this.commands[this.commands.length-1]){
                this.commands.push(command);
            }
            this.commands_index=this.commands.length-1;
            eval(command);
        }catch(e){
            this.log(' !'+e.message,true);
            //console.log(e);
        }
    }

    if(key=='End')this.x=this.matrix.x-1;
    if(key=='Home')this.x=0;
    if(key=='PageUp')this.y=0;
    if(key=='PageDown')this.y=this.matrix.y-1;
    
}

Cursor.prototype.events = function(key){

    if(key && key != '#')cursor.write(key);

    this.selectors[0]=this.matrix.lines[this.y].types[this.x];
    this.selectors[0].output=this.matrix.lines[this.y].types[this.x];

    if(window.Control){
        this.selectors[0].fill(this);
    }else{
        this.selectors[0].fill(this.selectors[0]);
    }

    if(key == '#' || key == '$'|| key == '%'|| key == '/'|| key == '\\'|| key == 'End'|| key == 'Home'|| key == 'PageUp'|| key == 'PageDown')this.key_events(key);

    if(this.keys.ArrowUp && this.y-1>-1)this.y--;
    if(this.keys.ArrowDown && this.y+1<this.matrix.y)this.y++;
    if(this.keys.ArrowLeft && this.x-1>-1)this.x--;
    if(this.keys.ArrowRight && this.x+1<this.matrix.x)this.x++;

    this.selectors[0]=this.matrix.lines[this.y].types[this.x];
    this.selectors[0].output=this.matrix.lines[this.y].types[this.x];
    
    update(this.selectors[0].dom,this.display);

}

Cursor.prototype.drop = function(){
    var selector = new Cursor({ink:'dimgrey',shoulder:'silver',face:'☻',x:this.x,y:this.y,matrix:this.matrix,display:{output:this.matrix.lines[this.y].types[this.x]}});
    selector.display.output.input=selector;
    selector.blink({cursor:selector,time:1500,dom:selector.display.output.dom,player:false,ink:'dimgrey',shoulder:'silver',face:'☻'});
    this.selectors.push(selector.display);
}

Cursor.prototype.reclaim = function(){
    for(var i in this.selectors){
        this.selectors[i].output.input=undefined;
        this.selectors[i].output.fill();
        delete this.selectors[i];
    }   
    this.selectors=[];
}
