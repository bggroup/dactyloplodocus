/*
                 ( floodfill.js )
               __ |/
              / _)
     _.----._/ /
    /         /
 __/ (  | (  |
/__.-'|_|--|_|

*/


/*

#Floodfill

…

*/


Floodfill = function (){
 this.speed = 100;
 this.stop = false;
}

/*

#Floodfill.sow();

…

*/

Floodfill.prototype.sow = function(options){

    if(this.stop)return;

    if(!options)options={};

    if(cursor.selectors.length>1){
        var c=1;
    }else{
        var c=0;
    }

    while(c<cursor.selectors.length){
        if(options.scope!='all')options.scope=[cursor.selectors[c].output.shoulder];
        options.type=cursor.selectors[c].output;
        this.grow(options,true);
        c++;
    }

}

/*

#Floodfill.grow();

algorithme récursif qui se diffuse dans les cellules voisines

*/

Floodfill.prototype.grow = function(options,_root){

    if(this.stop)return;

    var local={};

    define(local,options,{
        dir:[
        [-1,0],
        [+1,0],
        [0,-1],
        [0,+1],
        ],
        ink:undefined,
        face:undefined,
        shoulder:colors[random(0,colors.length).int],
        scope:undefined,
        type:undefined,
    });

    setTimeout(function() {

      if(local.type.shoulder!=local.shoulder){

        if(local.scope!='all'){
            var out_scope=true;
            for(var i in local.scope){
                if(local.type.shoulder==local.scope[i]){
                    out_scope=false;
                }
            }

            if(out_scope){
                return
            }
        }

        local.type.fill(local);

        var voisins=[];

        for(var i in local.dir){
            if(local.type.line.matrix.lines[local.type.line.id+local.dir[i][1]]){
                voisins.push(local.type.line.matrix.lines[local.type.line.id+local.dir[i][1]].types[local.type.id+local.dir[i][0]]);
            }
        }

        for(var i in voisins){
            if(voisins[i]){
                local.type=voisins[i];
                _floodfill.grow(local);
            }
        }

    }
}, this.speed);
    
}

var _floodfill = new Floodfill();

function floodfill(options){
    _floodfill.sow(options)
}

///////////////////////////////////

async function ffloodfill(options){

    if(stop){
        return;
    }
    if(caret.selectors.length>1){
        var c=1;
    }else{
        var c = 0;
    }
    for(c; c<caret.selectors.length; c++){

        var local={};

        define(local,options,{

            dir:[
            [-1,0],
            [+1,0],
            [0,-1],
            [0,+1],
            ],
            ink:undefined,
            face:undefined,
            face:faces[random(0,faces.length).int],
            //shoulder:cssColors[random(0,cssColors.length).int],
            shoulder:undefined,
            scope:[caret.selectors[c].output.face],
            type:caret.selectors[c].output,

        });

        

        await sleep(speed);

        if(local.type.face!=local.face){

            if(local.scope!='all'){

                var out_scope=true;
                for(var i in local.scope){
                    if(local.type.face==local.scope[i]){
                        out_scope=false;
                    }
                }
                if(out_scope){
                    return
                }
            }

            local.type.fill(local);

            var voisins=[];

            for(var i in local.dir){
                if(local.type.line.matrix.lines[local.type.line.id+local.dir[i][1]]){
                    voisins.push(local.type.line.matrix.lines[local.type.line.id+local.dir[i][1]].types[local.type.id+local.dir[i][0]]);
                }
            }

            for(var i in voisins){
                if(voisins[i]){
                    local.type=voisins[i];
                    ffloodfill(local);
                }
            }
        }
    }
}