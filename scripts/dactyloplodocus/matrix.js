/*
                 ( matrix.js )
               __ |/
              / _)
     _.----._/ /
    /         /
 __/ (  | (  |
/__.-'|_|--|_|

*/

Matrix = function(options){
    define(this,options,{
        lines:[],
        dom:document.createElement('table')
    });
}

Matrix.prototype.clear = async function(){
    for(var l in this.lines){
        for(var t in this.lines[l].types){
            this.lines[l].types[t].fill({face:' '});
        }
    }
}


Matrix.prototype.clean = async function(){
    for(var l in this.lines){
        for(var t in this.lines[l].types){
            this.lines[l].types[t].fill({shoulder:'black'});
        }
    }
}

Matrix.prototype.build = function(size){

    this.x=size[0];
    this.y=size[1];

    for(var y = 0 ; y < size[1] ; y++){

        var line = new Line({matrix:this,id:y});

        for(var x = 0 ; x < size[0] ; x++){

            var type = new Type({line:line,id:x});
            type.fill();
            line.types.push(type);
            line.dom.appendChild(type.dom);

        }

        this.lines.push(line);
        this.dom.appendChild(line.dom);

    }

}