/*
                 ( type.js )
               __ |/
              / _)
     _.----._/ /
    /         /
 __/ (  | (  |
/__.-'|_|--|_|

*/

Type = function(options){
    define(this,options,{
        id:undefined,
        shoulder:'transparent',
        face:' ',
        ink:'white',
        line:undefined,
        input:undefined,
        output:undefined,
        border:[
        [1,'transparent'],
        [1,'transparent'],
        [1,'transparent'],
        [1,'transparent'],
        ],
        dom:document.createElement('td')
    });
}

function update(dom,type){

    if(type.ink)dom.style.color=type.ink;
    if(type.face)dom.textContent=type.face;
    if(type.shoulder)dom.style.backgroundColor=type.shoulder;

}

Type.prototype.fill = function(options){
    define(this,options,this);
    if(this.input!=undefined){
        update(this.dom,this.input);
    }else{
        update(this.dom,this);
    }

}