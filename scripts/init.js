/*
                 ( init.js )
               __ |/
              / _)
     _.----._/ /
    /         /
 __/ (  | (  |
/__.-'|_|--|_|

*/

window.onmousedown=function(){
  var fontSize=30;
var html = document.all[0];
html.style.fontSize=fontSize+'px';
    if(!window.init){
        matrix = new Matrix();
        matrix.build([parseInt(window.innerWidth/(fontSize*0.7)),parseInt(window.innerHeight/(fontSize*1.5))]);

        cursor = new Cursor({matrix:matrix,x:0,y:0});
        cursor.init();
        cursor.events();

        document.body.appendChild(matrix.dom);
        var date = new Date();

        //cursor.log(String(date)+'€',false,50);
        window.init=true;
    }
}